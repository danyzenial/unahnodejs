

var table_show;
$(document).ready(function(){

  $.ajax({
    url:'/obtenerDatos2',
    method:'POST',
    success: function(respuesta){

          $("#nombreUsuario").html('<h4>'+respuesta[0].DATOS+'</h4>');
    }
  });

  $("#cerrarSession").click(function(){
      $.ajax({
        url:'/logout',
        success: function(){
              window.location.href='index.html';
          }
        });
  });


  $.ajax({
    url:'/cargarClasesActuales',
    method:'POST',
    success: function(respuesta){

          var contenido='';
          for (var i = 0; i < respuesta.length; i++) {
              contenido+='<h5><i class="fas fa-book"></i> '+respuesta[i].NOMBRE_ASIGNATURA+' '+respuesta[i].CODIGO_ALTERNO+'</h5><h6 style="cursor:pointer;" onclick="listarAlumnos('+respuesta[i].CODIGO_SECCION+');">Ver Alumnos <i class="fas fa-eye"></i></h6><br>';
          }

          $("#contenidoClases").html(contenido);
    }
  });

  table_show =  $('#table1').DataTable({
      dom: 'Bfrtip',
      buttons: [
        'excel',
        {extend: 'pdfHtml5',
          messageTop: 'Lista de Alumnos. Se recomienda imprimirla después de haber concluído el período de matrícula.',
          pageMargins: [ 150, 150, 150, 150 ],
        }
      ],
       responsive: true,
      "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }

});

});


function listarAlumnos(codigoSeccion){
        table_show
        .clear()
        .draw();
        var parametro='codSeccion='+codigoSeccion;
        $("#informacionEstudiantes").modal('show');
        $.ajax({
          url: '/cargarAlumnosSeccion',
          data:parametro,
          method:'POST',
          success: function(answer) {

                    for (var i = answer.length - 1; i >= 0; i--)
                    {
                        var rowNode = table_show
                        .row.add([
                                    answer[i].NUMERO_CUENTA,
                                    answer[i].NOMBRE,
                                    answer[i].APELLIDO,
                                    answer[i].TELEFONO,
                                    answer[i].CORREO_ELECTRONICO,
                                ])
                        .draw()
                        .node();
                    }

                  }

        });
}

function verificarIngresoCalificaciones(){
  $.ajax({
    url:'/verificarIngresoCalificaciones',
    data:'fechaActual='+fechaActual(),
    method:'POST',
    success: function(respuesta){
        if(respuesta.respuesta=='no'){
          swal("Todavía no está habilitado el ingreso de calificaciones", "Revise el calendario en la página principal", "warning")
        }else{
          window.location.href='ingresoCalificaciones.html';
        }
    }
  });
}


function fechaActual(){
      var d = new Date();

      var month = d.getMonth()+1;
      var day = d.getDate();

      var output = d.getFullYear() + '-' +
          ((''+month).length<2 ? '0' : '') + month + '-' +
          ((''+day).length<2 ? '0' : '') + day;

      return output
}
