
var codigoEmpleado;
var genero='cero';
var rol='cero';
var areasSolicitudes = [];
$(document).ready(function() {



  var table_show =  $('#table1').DataTable({

    /*"ajax": {"url":"../PHP/responses.php",
    "dataSrc": ""},
    "columns":[
      {"data":"NOMBRE_EMPLEADO"},
      {"data":"Apellido_EMPLEADO"},
      {"data":"NOMBRE_ROL"},
      {"data":"NOMBRE_USUARIO"},
      {"data":"TELEFONO_EMPLEADO"},
      {"data":"CORREO_EMPLEADO"},
      {"defaultContent":'<button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#editarUsuarioModal" class="btn btn-success"> Editar</button><button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#asignarTopicoModal" class="btn btn-primary"> Asignar Tópico</button><button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#infoUsuarioModal" class="btn btn-warning"> Info</button>' }
    ],*/
     responsive: true,
    "language": {

    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
  }
});

listar();

function listar(){

        var parametro='listarEmpleado='+1;
        $.ajax({
          url: '../PHP/Responses.php',
          method:'POST',
          data:parametro,
          dataType:'json',
          success: function(answer) {

                    for (var i = answer.length - 1; i >= 0; i--)
                    {
                        if(answer[i].NOMBRE_ROL=='tecnico'){
                        var rowNode = table_show
                        .row.add([
                                    answer[i].ID_EMPLEADO,
                                    answer[i].NOMBRE_EMPLEADO,
                                    answer[i].APELLIDO_EMPLEADO,
                                    answer[i].NOMBRE_ROL,
                                    answer[i].NOMBRE_USUARIO,
                                    answer[i].TELEFONO_EMPLEADO,
                                    answer[i].CORREO_EMPLEADO,
                                    '<button  data-toggle="modal" data-target="#adicionarAula" type="button" class="editar btn btn-primary btn-sm"><i class="fas fa-edit"></i> </button> '+
                                    '<button data-toggle="modal" data-target="#asignarTopicoModal" type="button" class="agregar btn btn-success  btn-sm" id=""><i class="fas fa-plus-circle"></i> </button> '+
                                    '<button data-toggle="modal" data-target="#actualizarPassword" type="button" class="actualizar btn btn-danger  btn-sm" id="eliminarRegistro1"><i class="fas fa-key"></i> </button>'

                                ])
                        .draw()
                        .node();
                      }else {
                        var rowNode = table_show
                        .row.add([
                                    answer[i].ID_EMPLEADO,
                                    answer[i].NOMBRE_EMPLEADO,
                                    answer[i].Apellido_EMPLEADO,
                                    answer[i].NOMBRE_ROL,
                                    answer[i].NOMBRE_USUARIO,
                                    answer[i].TELEFONO_EMPLEADO,
                                    answer[i].CORREO_EMPLEADO,
                                    '<button  data-toggle="modal" data-target="#ingresarUsuarioModal" type="button" class="editar btn btn-primary btn-sm"><i class="fas fa-edit"></i> </button> '+
                                    '<button data-toggle="modal" data-target="#editarUsuarioModal" type="button" class="cambiar btn btn-warning  btn-sm" id="infoUsuario"><i class="fas fa-key"></i> </button> '
                                ])
                        .draw()
                        .node();
                      }
                    }
                    //swal("Acción realizada con éxito!", "", "warning");

                  }

        });

          obtenerData("#table1 tbody",table_show);
          obtenerDataCambiar("#table1 tbody",table_show);
          obtenerDataTopico("#table1 tbody",table_show);

}

function obtenerData(tbody, table){
  $(tbody).on("click","button.editar",function(){
    $("#enviarEmpleado").attr('hidden', true);
    $("#campoContrasenia").attr('hidden',true);
    $("#actualizarEmpleado").removeAttr('hidden');
    var data = table.row($(this).parents("tr")).data();
    $("#nombreUsuario").val(data[3]);
    $("#nombresUsuario").val(data[1]);
    $("#apellidosUsuario").val(data[2]);
    $("#numeroIdentidad").val('');
    $("#telefono").val(data[5]);
    $("#email").val(data[6]);
    $("#ipTerminal").val('');
    console.log(data);
    codigoEmpleado=data[0];

  });
}

function obtenerDataCambiar(tbody, table){
  $(tbody).on("click","button.cambiar",function(){
    var data = table.row($(this).parents("tr")).data();
    console.log(data[0]);
    codigoEmpleado=data[0];
  });
}

function obtenerDataTopico(tbody, table){
  $("#topicos").html('');
  $(tbody).on("click","button.topicos",function(){
    var data = table.row($(this).parents("tr")).data();
    console.log(data[0]);
    codigoEmpleado=data[0];
    cargarTopicos(codigoEmpleado);
  });
}


});




var genero='cero';
var rol='cero';
$("#enviarEmpleado").click(function(){
  if($("#nombreUsuario").val()=='' || (/^[a-z0-9_.-]{3,15}$/.test($("#nombreUsuario").val()))!=true){
    $("#nombreUsuario").addClass('is-invalid');
  }else if ($("#contrasenia").val()=='' || (/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test($("#contrasenia").val()))!=true) {
    $("#nombreUsuario").removeClass('is-invalid');
    $("#contrasenia").addClass('is-invalid');
  }else if ($("#nombresUsuario").val()=='' || (/^([a-z ñáéíóú]{4,60})$/i.test($("#nombresUsuario").val()))!=true) {
    $("#contrasenia").removeClass('is-invalid');
    $("#nombresUsuario").addClass('is-invalid');
  }else if ($("#apellidosUsuario").val()=='' || (/^([a-z ñáéíóú]{4,60})$/i.test($("#apellidosUsuario").val()))!=true) {
    $("#nombresUsuario").removeClass('is-invalid');
    $("#apellidosUsuario").addClass('is-invalid');
  }else if ($("#numeroIdentidad").val()=='' || (/^[0-9][0-9][0-9][0-9]-\d{4}-\d{5}$/.test($("#numeroIdentidad").val()))!=true) {
    $("#numeroIdentidad").addClass('is-invalid');
    $("#apellidosUsuario").removeClass('is-invalid');
  }else if ($("#telefono").val()=='' || (/^[2-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/.test($("#telefono").val()))!=true) {
    $("#telefono").addClass('is-invalid');
    $("#numeroIdentidad").removeClass('is-invalid');
  }else if ($("#email").val()=='' || (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($("#email").val()))!=true) {
    $("#email").addClass('is-invalid');
    $("#telefono").removeClass('is-invalid');
  }else if ($("#ipTerminal").val()=='' || (/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/.test($("#ipTerminal").val()))!=true) {
    $("#ipTerminal").addClass('is-invalid');
    $("#email").removeClass('is-invalid');
  }else if (genero=='cero') {
    $("#genero").addClass('is-invalid');
    $("#ipTerminal").removeClass('is-invalid');
  }else if (rol=='cero') {
    $("#rol").addClass('is-invalid');
    $("#ipTerminal").removeClass('is-invalid');
  }else{
    $("#rol").removeClass('is-invalid');
    $("#nombreUsuario").removeClass('is-invalid');
    $("#contrasenia").removeClass('is-invalid');
    $("#nombresUsuario").removeClass('is-invalid');
    $("#apellidosUsuario").removeClass('is-invalid');
    $("#numeroIdentidad").removeClass('is-invalid');
     $("#telefono").removeClass('is-invalid');
     $("#email").removeClass('is-invalid');
      $("#ipTerminal").removeClass('is-invalid');

      guardarEmpleado();
  }
});

    $("#actualizarEmpleado").click(function(){



        if($("#nombreUsuario").val()=='' || (/^[a-z0-9_.-]{3,15}$/.test($("#nombreUsuario").val()))!=true){
          $("#nombreUsuario").addClass('is-invalid');
        }else if ($("#nombresUsuario").val()=='' || (/^([a-z ñáéíóú]{4,60})$/i.test($("#nombresUsuario").val()))!=true) {
          $("#nombreUsuario").removeClass('is-invalid');
          $("#nombresUsuario").addClass('is-invalid');
        }else if ($("#apellidosUsuario").val()=='' || (/^([a-z ñáéíóú]{4,60})$/i.test($("#apellidosUsuario").val()))!=true) {
          $("#nombresUsuario").removeClass('is-invalid');
          $("#apellidosUsuario").addClass('is-invalid');
        }else if ($("#numeroIdentidad").val()=='' || (/^[0-9][0-9][0-9][0-9]-\d{4}-\d{5}$/.test($("#numeroIdentidad").val()))!=true) {
          $("#numeroIdentidad").addClass('is-invalid');
          $("#apellidosUsuario").removeClass('is-invalid');
        }else if ($("#telefono").val()=='' || (/^[2-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/.test($("#telefono").val()))!=true) {
          $("#telefono").addClass('is-invalid');
          $("#numeroIdentidad").removeClass('is-invalid');
        }else if ($("#email").val()=='' || (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($("#email").val()))!=true) {
          $("#email").addClass('is-invalid');
          $("#telefono").removeClass('is-invalid');
        }else if ($("#ipTerminal").val()=='' || (/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/.test($("#ipTerminal").val()))!=true) {
          $("#ipTerminal").addClass('is-invalid');
          $("#email").removeClass('is-invalid');
        }else if (genero=='cero') {
          $("#genero").addClass('is-invalid');
          $("#ipTerminal").removeClass('is-invalid');
        }else if (rol=='cero') {
          $("#rol").addClass('is-invalid');
          $("#ipTerminal").removeClass('is-invalid');
        }else{
          $("#rol").removeClass('is-invalid');
          $("#nombreUsuario").removeClass('is-invalid');
          $("#nombresUsuario").removeClass('is-invalid');
          $("#apellidosUsuario").removeClass('is-invalid');
          $("#numeroIdentidad").removeClass('is-invalid');
           $("#telefono").removeClass('is-invalid');
           $("#email").removeClass('is-invalid');
            $("#ipTerminal").removeClass('is-invalid');

            actualizarEmpleado();
            table_show.clear();
            listar();
        }



    });

  $("#nombreUsuario").keyup(function(){
    //console.log('Estás escribiendo...');
    var nombre=$("#nombreUsuario").val()
    console.log(nombre);
    var parametro3 = "verificarUsuario="+nombre;






    $.ajax({
        url: '../PHP/Registrar.php',
        method: 'POST',
        data: parametro3,
        success: function(respuestaNueva) {
          if(respuestaNueva=='0'){
              $("#incorrectoNombreUsuario").attr('hidden',true);
              console.log(respuestaNueva);
              }else{
              $("#incorrectoNombreUsuario").removeAttr('hidden');
            }
          }

    });

  });


  $("#genero").change(function(){
        genero=$('select[id=genero]').val();
    });


    $("#rol").change(function(){
          rol=$('select[id=rol]').val();
      });


$("#ipTerminal").keyup(function(){
    if($("#ipTerminal").val() != ''){
    $.ajax({
        url: '../PHP/Registrar.php',
        method: 'POST',
        data: "verificarIP="+$("#ipTerminal").val(),
        success: function(respuestaNueva) {
          if(respuestaNueva=='1'){
              $("#ipTerminal + div + br").next().text('IP Existente!');
              $("#ipTerminal").addClass('is-invalid');
              }else{
              $("#ipTerminal").removeClass('is-invalid');
              $("#ipTerminal + div + br").next().text('Campo Obligatorio');
            }
          }

    });
  }

  });


  function actualizarEmpleado(){

  var datos =  '{"nombreUsuario":"'+$("#nombreUsuario").val()+
  '","nombres":"'+$("#nombresUsuario").val()+
  '","apellidos":"'+$("#apellidosUsuario").val()+
  '","identidad":"'+$("#numeroIdentidad").val()+
  '","telefono":"'+$("#telefono").val()+
  '","correo":"'+$("#email").val()+
  '","genero":"'+genero+
  '","ip":"'+$("#ipTerminal").val()+
  '","rol":'+rol+
  '}';
    $.ajax({
        url: '../PHP/Registrar.php',
        method: 'POST',
        data: {gEmpleado: JSON.parse(datos)},
        success: function(respuesta) {
          if(respuesta=='correcto'){
              table_show.clear();
                listar();
                swal("Empleado Registrado con éxito", "", "success");
                $("#nombreUsuario").val('');
                $("#nombresUsuario").val('');
                $("#apellidosUsuario").val('');
                $("#numeroIdentidad").val('');
                $("#telefono").val('');
                $("#email").val('');
                $("#ipTerminal").val('');
              }else{
                swal("No se ha podido guardar empleado", "", "error");
            }
          }
        }
    );

  }

function guardarEmpleado(){

var datos =  '{"nombreUsuario":"'+$("#nombreUsuario").val()+
'","contra":"'+$("#contrasenia").val()+
'","nombres":"'+$("#nombresUsuario").val()+
'","apellidos":"'+$("#apellidosUsuario").val()+
'","identidad":"'+$("#numeroIdentidad").val()+
'","telefono":"'+$("#telefono").val()+
'","correo":"'+$("#email").val()+
'","genero":"'+genero+
'","ip":"'+$("#ipTerminal").val()+
'","rol":'+rol+
'}';
  $.ajax({
      url: '../PHP/Registrar.php',
      method: 'POST',
      data: {gEmpleado: JSON.parse(datos)},
      success: function(respuesta) {
        if(respuesta=='correcto'){
              swal("Empleado Registrado con éxito", "", "success");
              $("#nombreUsuario").val('');
              $("#contrasenia").val('');
              $("#nombresUsuario").val('');
              $("#apellidosUsuario").val('');
              $("#numeroIdentidad").val('');
              $("#telefono").val('');
              $("#email").val('');
              $("#ipTerminal").val('');
            }else{
              swal("No se ha podido guardar empleado", "", "error");
          }
        }
      }
  );

}


$("#enviarTopico").click(function(){
  if($("#nombreTopico").val()==''){
    $("#nombreTopico").addClass('is-invalid');
  }else{
    $("#nombreTopico").removeClass('is-invalid');
      guardarTopico();
    $("#nombreTopico").val('');

  }
});

function guardarTopico(){

    $.ajax({
        url: '../PHP/Registrar.php',
        method: 'POST',
        data: "guardarTopico="+$("#nombreTopico").val(),
        success: function(respuesta) {
        if(respuesta=='correcto'){
              swal("Tópico ingresado correctamente", "", "success");
            }else{
              swal("No se ha podido guardar tópico", "", "error");
            }

            }
        }
    );

}


$("#actualizarEnviarContrasenia").click(function(){
  if ($("#actualizarContrasenia").val()=='' || (/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test($("#actualizarContrasenia").val()))!=true) {
    $("#actualizarContrasenia").addClass('is-invalid');
  }else{
    $("#actualizarContrasenia").removeClass('is-invalid');
    var parametro = 'codigoEmpleado='+codigoEmpleado+'&=nuevaContrasenia='+$("#actualizarContrasenia").val();
    alert(parametro);
    /*
    $.ajax({
        url: '../PHP/topicos.php',
        success: function(respuesta) {
           var topicos = respuesta.split(',');
           for (var i =0 ; i< topicos.length; i++) {
               $("#topicos").append(format(topicos[i]));
           };
        }
    });*/

  }
});


$("#enviarTopicoUsuario").click(function(){
  var parametro='codigoEmpleado='+codigoEmpleado+'&topicos='+areasSolicitudes;
  if(codigoEmpleado!='' && areasSolicitudes.length!=0){
    /*$.ajax({
        url: '../PHP/topicos.php',
        success: function(respuesta) {
           var topicos = respuesta.split(',');
           for (var i =0 ; i< topicos.length; i++) {
               $("#topicos").append(format(topicos[i]));
           };
        }
    });*/
  }else{
    swal("Debe seleccionar al menos un tópico", "Vuelva a intentar nuevamente", "error");
  }
});

function cargarTopicos(codigoEmpleado) {

  var parametro='codigoEmpleado='+codigoEmpleado;
    $.ajax({
        url: '../PHP/topicos.php',
        success: function(respuesta) {
           var topicos = respuesta.split(',');
           for (var i =0 ; i< topicos.length; i++) {
               $("#topicos").append(format(topicos[i]));
           };
        }
    });

}

function format(topico){
         var estilo = "<tr><td>"+topico+"</td><td><button type=\"button\" id=\""+topico+"\" name=\"button\" class=\"btn btn-primary\" onclick=\"areasSolicitud(\'"+topico+"\',\'cancelar"+topico+"\')\">Agregar <i class=\"fas fa-plus-circle\"></i></button><button id=\"cancelar"+topico+"\" type=\"button\" name=\"button\" class=\"btn btn-danger\" style=\"display:none;\" onclick=\"removerAreaSolicitud(\'"+topico+"\',\'cancelar"+topico+"\')\">Eliminar <i class=\"fas fa-trash-alt\"></i></button></td></tr>";
         return estilo;
}

function areasSolicitud(servicio, cancelar) {
    alert(servicio);
    $("#" + servicio).css("display", "none");
    $("#" + cancelar).css("display", "block");
    areasSolicitudes.push(servicio)
    areasSolicitudes.toString();
    document.getElementById("serviciosSeleccionados").innerHTML = areasSolicitudes;

}

function removerAreaSolicitud(servicio, cancelar) {
    $("#" + servicio).css("display", "block");
    $("#" + cancelar).css("display", "none");
    var index = areasSolicitudes.indexOf(servicio);
    if (index > -1) {
        areasSolicitudes.splice(index, 1);
    }
    areasSolicitudes.toString();
    document.getElementById("serviciosSeleccionados").innerHTML = areasSolicitudes;
}

$("#cerrarModal").click(function(){
  $("#campoContrasenia").removeAttr('hidden');
  $("#enviarEmpleado").removeAttr('hidden');
  $("#actualizarEmpleado").attr('hidden', true);
});
