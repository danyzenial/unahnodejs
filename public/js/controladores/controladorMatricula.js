
var seccionCancelar;

cargarDepartamentos();
var table_show =  $('#table1').DataTable({
  "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ],
   responsive: true,
  "language": {

  "sProcessing":     "Procesando...",
  "sLengthMenu":     "Mostrar _MENU_ registros",
  "sZeroRecords":    "No se encontraron resultados",
  "sEmptyTable":     "Ningún dato disponible en esta tabla",
  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
  "sInfoPostFix":    "",
  "sSearch":         "Buscar:",
  "sUrl":            "",
  "sInfoThousands":  ",",
  "sLoadingRecords": "Cargando...",
  "oPaginate": {
      "sFirst":    "Primero",
      "sLast":     "Último",
      "sNext":     "Siguiente",
      "sPrevious": "Anterior"
  },
  "oAria": {
      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  }
}
});

listar();

function listar(){

      $.ajax({
        url: '/cargarMatricula',
        method:'POST',
        success: function(answer) {

                  for (var i = answer.length - 1; i >= 0; i--)
                  {
                      var rowNode = table_show
                      .row.add([
                                  answer[i].CODIGO_SECCION,
                                  answer[i].CODIGO_ALTERNO,
                                  answer[i].NOMBRE_ASIGNATURA,
                                  answer[i].CANTIDAD_UNIDADES_VALORATIVAS,
                                  answer[i].SECCION,
                                  answer[i].HORA_INICIO,
                                  answer[i].HORA_FIN,
                                  answer[i].NUMERO_AULA,
                                  answer[i].NOMBRE_EDIFICIO,
                                  answer[i].Docente,
                                  '<button  type="button" onclick="eliminarMatricula('+answer[i].CODIGO_SECCION+')" class="eliminar btn btn-danger  btn-sm"><i class="fas fa-trash-alt"></i></button>'
                              ])
                      .draw()
                      .node();
                  }

                }

      });

      obtenerData("#table1 tbody",table_show);

}

function obtenerData(tbody, table){
  $(tbody).on("click","button.eliminar",function(){
    var data = table.row($(this).parents("tr")).data();
    seccionCancelar=data[0];
  });
}



function eliminarMatricula(seccionCancelar){

        swal({
              title: "¿Realmente Seguro?",
              text: "Una vez cancelada, usted perderá su cupo, y tendrá que volver a matricular.",
              icon: "warning",
              buttons: true,
              dangerMode: true,
        })
        .then((willDelete) => {
              if (willDelete) {

              var parametro='codigoSeccion='+seccionCancelar;
              $.ajax({
                url:'/cancelarMatricula',
                data:parametro,
                method:'POST',
                success: function(respuesta){
                      if(respuesta=='correcto'){
                        table_show.clear();
                        listar();
                        swal("¡Asignatura cancelada con éxito!", "Éxitos", "success");
                      }else{
                        swal("¡Ha ocurrido un error", "Vuelva a intentar", "error");
                      }
                }
              });

              } else {
                swal("Asignatura no cancelada.");
              }
            });


}



$("#enviarMatricula").attr('disabled', true);
var codSeccion;

function obtenerClases(codigoDepartamento){
    $("#enviarMatricula").attr('disabled', true);
  $("#clase").html('');
  $.ajax({
    url:'/slcClases',
    method:'POST',
    data:'codigoDepartamento='+codigoDepartamento,
    success: function(respuesta){
      var contenido;
      for (var i = 0; i < respuesta.length; i++) {
          contenido+='<option onclick="verificarRequisitos('+respuesta[i].CODIGO_ASIGNATURA+')">'+respuesta[i].NOMBRE_CLASE+'</option>';
          }

    $("#Clase").html(contenido);
    }
  });
}

function obtenerSecciones(codigoClase){
    $("#enviarMatricula").attr('disabled', true);
  $("#Seccion").html('');
  $.ajax({
    url:'/slcSecciones',
    method:'POST',
    data:'codigoClase='+codigoClase,
    success: function(respuesta){
      var contenido;
      for (var i = 0; i < respuesta.length; i++) {
          contenido+='<option onclick="matricular('+respuesta[i].CODIGO_ASIGNATURA+','+respuesta[i].CODIGO_SECCION+','+respuesta[i].CANTIDAD_CUPOS+')">'+'Sección: '+respuesta[i].CODIGO_ALTERNO+' '+'Días: '+respuesta[i].DIAS+' Cupos: '+respuesta[i].CANTIDAD_CUPOS+' Docente: '+respuesta[i].DOCENTE+'</option>';
          }

    $("#Seccion").html(contenido);
    }
  });

}


function cargarDepartamentos(){
    $("#enviarMatricula").attr('disabled', true);
  $.ajax({
    url:'/slcDepartamentos',
    success: function(respuesta){
        var contenido;

        for (var i = 0; i < respuesta.length; i++) {
		        contenido+='<option onclick="obtenerClases('+respuesta[i].CODIGO_DEPARTAMENTO+')">'+respuesta[i].NOMBRE_CARRERA+'</option>';
            }
        $("#Departamento").html(contenido);

    }
  });
}

function verificarRequisitos(codigoAsignatura){
    $("#enviarMatricula").attr('disabled', true);
    $.ajax({
    url:'/verificarRequisitos',
    data:'codigoAsignatura='+codigoAsignatura,
    method:'POST',
    success: function(respuesta){

        if(respuesta[0].requisitos=='0'){

            obtenerSecciones(codigoAsignatura);
        }else{
          swal("Faltan requisitos", "Revise su plan de estudio", "warning");
        }
    }
  });

}




function matricular(asignatura,codigoSeccion, cupos){
      $("#enviarMatricula").attr('disabled', true);
      $.ajax({
      url:'/verificarExistencia',
      data:'codigoAsignatura='+asignatura,
      method:'POST',
      success: function(respuesta){
          if(respuesta[0].existe==0){

            if(cupos>0){
              $("#enviarMatricula").removeAttr('disabled');
              codSeccion=codigoSeccion;
            }else{
                swal("¡Sección Llena!", "Estar pendiente de nuevas secciones y aumentos de cupos", "info");
                $("#enviarMatricula").attr('disabled', true);
            }

          }else{
            swal("Está clase ya se encuentra matrículada", "Revise su FORMA 03", "warning");
          }
      }
    });

}


$("#enviarMatricula").click(function(){

        $.ajax({
        url:'/guardarMatricula',
        data:'codSeccion='+codSeccion,
        method:'POST',
        success: function(respuesta){
            if(respuesta=='correcto'){
              $("#Seccion").html('');
              swal("¡Asignatura matrículada con éxito!", "Éxitos", "success");
              table_show.clear();
              listar();

            }else{
                swal("¡Ha ocurrido un error!", "Vuelva a intentar", "error");
            }
        }
      });

});
