$(document).ready(function(){

  var table_show =  $('#table1').DataTable({
      dom: 'Bfrtip',
      buttons: [
        'excel',
        {extend: 'pdfHtml5',
          messageTop: 'Historial Académico Válido solo de orientación, para trámites, debe ir a registro a solicitar uno.',
          pageMargins: [ 150, 150, 150, 150 ],
        }
      ],
       responsive: true,
      "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }

});

listar();

function listar(){

      $.ajax({
        url: '/cargarPlanEstudio',
        success: function(answer) {

                  for (var i = answer.length - 1; i >= 0; i--)
                  {
                      var requisitos='';
                      for (var j = 0; j < answer[i].clases.length; j++) {
                        requisitos+=answer[i].clases[j].NOMBRE_ASIGNATURA+'  ';
                      }

                      var rowNode = table_show
                      .row.add([
                                  answer[i].NOMBRE_CARRERA,
                                  answer[i].CODIGO_ALTERNO,
                                  answer[i].NOMBRE_ASIGNATURA,
                                  answer[i].CANTIDAD_UNIDADES_VALORATIVAS,
                                  requisitos
                              ])
                      .draw()
                      .node();
                  }

                }

      });
}


});
