
$(document).ready(function(){
  $.ajax({
    url:'/cargarClasesActuales',
    method:'POST',
    success: function(respuesta){

          var contenido='<option disabled selected>Seleccionar Clase</option>';
          for (var i = 0; i < respuesta.length; i++) {
              contenido+='<option value='+respuesta[i].CODIGO_SECCION+'>'+respuesta[i].NOMBRE_ASIGNATURA+' '+respuesta[i].CODIGO_ALTERNO+'</option>';
          }

          $("#clases").html(contenido);
    }
  });

  $.ajax({
    url:'/cargarObs',
    success: function(respuesta){

          var contenido='<option disabled selected >Observación</option>';
          for (var i = 0; i < respuesta.length; i++) {
              contenido+='<option value='+respuesta[i].CODIGO_TIPO_EVALUACION+'>'+respuesta[i].NOMBRE_TIPO_EVALUACION+'</option>';
          }

          $("#obs").html(contenido);
    }
  });




});

var alumno,seccion,periodo;
var table_show =  $('#table1').DataTable({
      "columnDefs": [
      {
          "targets": [ 0 ],
          "visible": false,
          "searchable": false
      },
      {
          "targets": [ 1 ],
          "visible": false
      },
      {
          "targets": [ 2 ],
          "visible": false
      }
    ],
    dom: 'Bfrtip',
    buttons: [
      'excel',
      {extend: 'pdfHtml5',
        messageTop: 'Historial Académico Válido solo de orientación, para trámites, debe ir a registro a solicitar uno.',
        pageMargins: [ 150, 150, 150, 150 ],
      }
    ],
     responsive: true,
    "language": {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
  }

});


$("#clases").change(function(){
  table_show
  .clear()
  .draw();
  listarTabla();
});

function obtenerData(tbody, table){
  $(tbody).on("click","button.editar",function(){
    var data = table.row($(this).parents("tr")).data();
    console.log(data);
    $("#nombreCampus").val(data[1]);
    alumno=data[0];
    seccion=data[1];
    periodo=data[2];
  });
}


$("#enviarNota").click(function(){
  if($("#nota").val()=='' ||  $("#obs").val()==null || $("#obs").val()==null){
    swal("Debe llenar todos los campos", "", "info");
  }else{
      var parametro='alumno='+alumno+'&seccion='+seccion+'&periodo='+periodo
                    +'&tipoEvaluacion='+$("#obs").val()+'&nota='+$("#nota").val();
      $.ajax({
        url:'/actualizarMatricula',
        data:parametro,
        method:'POST',
        success: function(respuesta){
           if(respuesta=='correcto'){
             table_show
             .clear()
             .draw();
             listarTabla();
               swal("Nota ha sigo ingresada correctamente", "", "success");
           }else{
             swal("Ha ocurrido un error", "Intentar nuevamente", "error");
           }
        }
      });
  }
});


function listarTabla(){
  var parametro='codSeccion='+$("#clases").val();
  $.ajax({
    url: '/cargarAlumnosSeccion2',
    data:parametro,
    method:'POST',
    success: function(answer) {

              for (var i = answer.length - 1; i >= 0; i--)
              {
                  var rowNode = table_show
                  .row.add([
                              answer[i].CODIGO_ALUMNO,
                              answer[i].CODIGO_SECCION,
                              answer[i].CODIGO_PERIODO,
                              answer[i].NUMERO_CUENTA,
                              answer[i].NOMBRE,
                              answer[i].APELLIDO,
                              answer[i].TELEFONO,
                              answer[i].CORREO_ELECTRONICO,
                              answer[i].NOTA_TEMPORAL,
                              answer[i].EVALUACION_TEMPORAL,
                              '<button  data-toggle="modal" data-target="#subirNota" type="button" class="editar btn btn-success btn-sm"><i class="fas fa-arrow-alt-circle-up"></i></button>'
                          ])
                  .draw()
                  .node();
              }
              obtenerData("#table1 tbody",table_show);

            }

  });
}
