

var table_show =  $('#table1').DataTable({
  "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ],
   responsive: true,
  "language": {

  "sProcessing":     "Procesando...",
  "sLengthMenu":     "Mostrar _MENU_ registros",
  "sZeroRecords":    "No se encontraron resultados",
  "sEmptyTable":     "Ningún dato disponible en esta tabla",
  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
  "sInfoPostFix":    "",
  "sSearch":         "Buscar:",
  "sUrl":            "",
  "sInfoThousands":  ",",
  "sLoadingRecords": "Cargando...",
  "oPaginate": {
      "sFirst":    "Primero",
      "sLast":     "Último",
      "sNext":     "Siguiente",
      "sPrevious": "Anterior"
  },
  "oAria": {
      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  }
}
});

listar();

function listar(){

      $.ajax({
        url: '/cargarMatricula',
        method:'POST',
        success: function(answer) {

                  for (var i = answer.length - 1; i >= 0; i--)
                  {
                      var rowNode = table_show
                      .row.add([
                                  answer[i].CODIGO_SECCION,
                                  answer[i].CODIGO_ALTERNO,
                                  answer[i].NOMBRE_ASIGNATURA,
                                  answer[i].CANTIDAD_UNIDADES_VALORATIVAS,
                                  answer[i].SECCION,
                                  answer[i].HORA_INICIO,
                                  answer[i].HORA_FIN,
                                  answer[i].Docente,
                                  answer[i].NOTA_TEMPORAL,
                                  answer[i].NOMBRE_TIPO_EVALUACION
                              ])
                      .draw()
                      .node();
                  }

                }

      });


}
