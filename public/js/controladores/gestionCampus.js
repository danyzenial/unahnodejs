

$( document ).ready(function() {


  var codCampus;
  var table_show =  $('#table1').DataTable({

    /*"ajax": {"url":"../PHP/responses.php",
    "dataSrc": ""},
    "columns":[
      {"data":"NOMBRE_EMPLEADO"},
      {"data":"Apellido_EMPLEADO"},
      {"data":"NOMBRE_ROL"},
      {"data":"NOMBRE_USUARIO"},
      {"data":"TELEFONO_EMPLEADO"},
      {"data":"CORREO_EMPLEADO"},
      {"defaultContent":'<button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#editarUsuarioModal" class="btn btn-success"> Editar</button><button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#asignarTopicoModal" class="btn btn-primary"> Asignar Tópico</button><button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#infoUsuarioModal" class="btn btn-warning"> Info</button>' }
    ],*/
     responsive: true,
    "language": {

    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
  }
});

listar();

function listar(){

        $.ajax({
          url: '/cargarCampus',
          success: function(answer) {

                    for (var i = answer.length - 1; i >= 0; i--)
                    {
                        var rowNode = table_show
                        .row.add([
                                    answer[i].CODIGO_CAMPUS,
                                    answer[i].NOMBRE_CAMPUS,
                                    '<button  data-toggle="modal" data-target="#adicionarCampus" type="button" class="editar btn btn-primary btn-sm"><i class="fas fa-edit"></i> </button> '+
                                    '<button data-toggle="modal" data-target="#confirmarRespuesta" type="button" class="eliminar btn btn-danger  btn-sm" id="eliminarRegistro1"><i class="fas fa-trash-alt"></i></button>'
                                ])
                        .draw()
                        .node();
                    }

                  }

        });

      obtenerData("#table1 tbody",table_show);
      obtenerDataDelete("#table1 tbody",table_show);

}

function obtenerData(tbody, table){
  $(tbody).on("click","button.editar",function(){
    $("#actualizarCampus").removeAttr('hidden');
    $("#enviarCampus").attr('hidden',true);
    var data = table.row($(this).parents("tr")).data();
    console.log(data);
    $("#nombreCampus").val(data[1]);
    codCampus=data[0];

  });
}

function obtenerDataDelete(tbody, table){
  $(tbody).on("click","button.eliminar",function(){
    var data = table.row($(this).parents("tr")).data();
    console.log(data[0]);
    codCampus=data[0];
  });
}


function guardarCampus(){
  var parametros = "nombreCampus="+$("#nombreCampus").val();
  $.ajax({
        url:"/guardarCampus",
        method:'POST',
        data:parametros,
        success:function(respuesta){
          table_show.clear();
          listar();
              console.log(respuesta);

        }
        });
}

$("#actualizarCampus").click(function(){

  var parametros = 'codigoCampus='+codCampus+"&nombreCampus="+$("#nombreCampus").val();

  $.ajax({
    url: '/actualizarCampus',
    method:'POST',
    data:parametros,
    success: function(answer) {
      table_show.clear();
      listar();
      $("#nombreCampus").val('');

        }
  })





});

$("#cerrarModal").click(function(){
  $("#enviarCarrera").removeAttr('hidden');
  $("#actualizar").attr('hidden',true);
});

$("#modal-btn-si").on("click", function(){
  $("#confirmarRespuesta").modal('hide');
  var parametros='codigoCampus='+codCampus;
  $.ajax({
    url: '/eliminarCampus',
    method:'POST',
    data:parametros,
    success: function(answer) {
          table_show.clear();
          listar();
        }

  });

});

$("#modal-btn-no").on("click", function(){
  $("#confirmarRespuesta").modal('hide');
});


$("#cerrarModal").click(function(){
  $("#enviarCampus").removeAttr('hidden');
  $("#actualizarCampus").attr('hidden',true)
  $("#nombreCampus").val('');
});


$("#enviarCampus").click(function(){
  if($("#nombreCampus").val()==''){
    $("#nombreCampus").addClass('is-invalid');
  }else{
      $("#nombreCampus").removeClass('is-invalid');
      guardarCampus();
      table_show.clear();
      listar();
      $("#nombreCampus").val('');
  }
});


});
