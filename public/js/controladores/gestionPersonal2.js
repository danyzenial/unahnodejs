

$(document).ready(function(){

var codigoPersona;
/////////////////////////AJAX

$.ajax({
  url:'/slcTitularidad',
  success: function(answer){
    var contenido='<option value="0" selected disabled>Titularidad*</option>';
    for (var i = answer.length - 1; i >= 0; i--)
    {
        contenido+='<option value="'+answer[i].CODIGO_TITULARIDAD+'">'+answer[i].NOMBRE_TITULARIDAD+'</option>'
    }
    $("#titularidadDocente").html(contenido);

  }

});

$.ajax({
  url:'/slcEspecialidad',
  success: function(answer){
    var contenido='<option value="0" selected disabled>Especialización*</option>';
    for (var i = answer.length - 1; i >= 0; i--)
    {
        contenido+='<option value="'+answer[i].CODIGO_ESPECIALIZACION+'">'+answer[i].DESCRIPCION+'</option>'
    }
    $("#especializacionDocente").html(contenido);

  }

});


$.ajax({
  url:'/slcGenero',
  success: function(answer){
    var contenido='<option value="0" selected disabled>Género*</option>';
    for (var i = answer.length - 1; i >= 0; i--)
    {
        contenido+='<option value="'+answer[i].CODIGO_GENERO+'">'+answer[i].NOMBRE_GENERO+'</option>'
    }
    $("#generoPersona").html(contenido);

  }

});

$.ajax({
  url:'/slcTipoEmpleado',
  success: function(answer){
    var contenido='<option value="0" selected disabled>Tipo Empleado*</option>';
    for (var i = answer.length - 1; i >= 0; i--)
    {
        contenido+='<option value="'+answer[i].CODIGO_TIPO_EMPLEADO+'">'+answer[i].NOMBRE_TIPO_EMPLEADO+'</option>'
    }
    $("#tipoEmpleado").html(contenido);
    $("#tipoEmpleado1").html(contenido);

  }

});

$.ajax({
  url:'/slcTitulo',
  success: function(answer){
    var contenido='<option value="0" selected disabled>Título*</option>';
    for (var i = answer.length - 1; i >= 0; i--)
    {
        contenido+='<option value="'+answer[i].CODIGO_TITULO+'">'+answer[i].NOMBRE_TITULO+'</option>'
    }
    $("#codigoTitulo").html(contenido);

  }

});


$.ajax({
  url:'/slcUniversidad',
  success: function(answer){
    var contenido='<option value="0" selected disabled>Universidad*</option>';
    for (var i = answer.length - 1; i >= 0; i--)
    {
        contenido+='<option value="'+answer[i].CODIGO_UNIVERSIDAD+'">'+answer[i].NOMBRE_UNIVERSIDAD+'</option>'
    }
    $("#codigoUniversidad").html(contenido);

  }

});


$.ajax({
  url:'/slcCargo',
  success: function(answer){
    var contenido='<option value="0" selected disabled>Cargo*</option>';
    for (var i = answer.length - 1; i >= 0; i--)
    {
        contenido+='<option value="'+answer[i].CODIGO_CARGO+'">'+answer[i].NOMBRE_CARGO+'</option>'
    }
    $("#cargoPesonal").html(contenido);

  }

});

$.ajax({
  url:'/slcCivil',
  success: function(answer){
    var contenido='<option value="0" selected disabled>Estado Civil*</option>';
    for (var i = answer.length - 1; i >= 0; i--)
    {
        contenido+='<option value="'+answer[i].CODIGO_ESTADO_CIVIL+'">'+answer[i].NOMBRE_ESTADO_CIVIL+'</option>'
    }
    $("#estadoCivilPersona").html(contenido);

  }

});

$.ajax({
  url:'/slcIdentificacion',
  success: function(answer){
    var contenido='<option value="0" selected disabled>Tipo de Identificación*</option>';
    for (var i = answer.length - 1; i >= 0; i--)
    {
        contenido+='<option value="'+answer[i].CODIGO_TIPO_IDENTIFICACION+'">'+answer[i].TIPO_IDENTIFICACION+'</option>'
    }
    $("#documentacionPersona").html(contenido);

  }

});

$.ajax({
  url:'/slcCampus',
  success: function(answer){
    var contenido='<option value="0" selected disabled>Campus*</option>';
    for (var i = answer.length - 1; i >= 0; i--)
    {
        contenido+='<option value="'+answer[i].CODIGO_CAMPUS+'">'+answer[i].NOMBRE_CAMPUS+'</option>'
    }
    $("#campusPersona").html(contenido);

  }

});

$.ajax({
  url:'/slcLugar',
  success: function(answer){
    var contenido;
    for (var i = answer.length - 1; i >= 0; i--)
    {
        contenido+='<option value="'+answer[i].CODIGO_LUGAR+'">'+answer[i].NOMBRE_LUGAR+'</option>'
    }
    $("#lugarNacimientoPersona").append(contenido);
    $("#lugarResidenciaPersona").append(contenido);
    $("#lugarResidenciaPersona1").append(contenido);


  }

});

$("#lugarResidenciaPersona1").change(function(){
  var parametros = 'codigoLugar='+$("#lugarResidenciaPersona1").val();
  $.ajax({
    url: '/slcMunicipio',
    method:'POST',
    data:parametros,
    success: function(answer) {

              var contenido='<option value="0" selected disabled>Municipio de Residencia</option>';
              for (var i = answer.length - 1; i >= 0; i--)
              {
                  contenido+='<option value="'+answer[i].CODIGO_LUGAR+'">'+answer[i].NOMBRE_LUGAR+'</option>'
              }
              $("#municipioResidenciaPersona1").html(contenido);

            }

  });
});


$("#lugarNacimientoPersona").change(function(){
  var parametros = 'codigoLugar='+$("#lugarNacimientoPersona").val();
  $.ajax({
    url: '/slcMunicipio',
    method:'POST',
    data:parametros,
    success: function(answer) {

              var contenido='<option value="0" selected disabled>Municipio de Nacimiento</option>';
              for (var i = answer.length - 1; i >= 0; i--)
              {
                  contenido+='<option value="'+answer[i].CODIGO_LUGAR+'">'+answer[i].NOMBRE_LUGAR+'</option>'
              }
              $("#municipioNacimientoPersona").html(contenido);

            }

  });
});

$("#lugarResidenciaPersona").change(function(){
  var parametros = 'codigoLugar='+$("#lugarResidenciaPersona").val();
  $.ajax({
    url: '/slcMunicipio',
    method:'POST',
    data:parametros,
    success: function(answer) {

              var contenido='<option value="0" selected disabled>Municipio de Residencia</option>';
              for (var i = answer.length - 1; i >= 0; i--)
              {
                  contenido+='<option value="'+answer[i].CODIGO_LUGAR+'">'+answer[i].NOMBRE_LUGAR+'</option>'
              }
              $("#municipioResidenciaPersona").html(contenido);

            }

  });
});

//////////////////////////FUNCIONES

$("#numeroEmpleado").focusout(function(){
   var parametros = 'nEmpleado='+$("#numeroEmpleado").val();

   $.ajax({
     url: '/verificarCuentaEmpleado',
     method:'POST',
     data:parametros,
     success: function(answer) {
         if(answer.length>0){
           swal("Número de Empleado ya Existe!", "Ingreselo correctamente", "error");
           }
     }
   });

 });



function guardarPersonal(){
  var personal =    'codigoGenero='+$("#generoPersona").val()+
                    '&lugarNacimiento='+$("#lugarNacimientoPersona").val()+
                    '&lugarResidencia='+$("#lugarResidenciaPersona").val()+
                    '&municioNacimiento='+$("#municipioNacimientoPersona").val()+
                    '&municioResidencia='+$("#municipioResidenciaPersona").val()+
                    '&campus='+$("#campusPersona").val()+
                    '&identificacion='+$("#documentacionPersona").val()+
                    '&estadoCivil='+$("#estadoCivilPersona").val()+
                    '&nombre='+$("#nombresPersona").val()+
                    '&apellido='+$("#apellidosPersona").val()+
                    '&identidad='+$("#idPersona").val()+
                    '&direccion='+$("#direccionPersona").val()+
                    '&telefono='+$("#telefonoPersona").val()+
                    '&email='+$("#correoPersona").val()+
                    '&numeroEmpleado='+$("#numeroEmpleado").val()+
                    '&contrasenia='+$("#contrasenia").val()+
                    '&sueldo='+$("#saldoBase").val()+
                    '&codigoTitulo='+$("#codigoTitulo").val()+
                    '&codigoUniversidad='+$("#codigoUniversidad").val()+
                    '&fechaTitulo='+$("#fechaTitulo").val()+
                    '&numeroRegistro='+$("#numeroRegistro").val()+
                    '&cargoPesonal='+$("#cargoPesonal").val()+
                    '&codigotitularidad='+$("#titularidadDocente").val()+
                    '&codigoEspecialidad='+$("#especializacionDocente").val();




                    $.ajax({
                      url: '/guardarPersonal1',
                      method:'POST',
                      data:personal,
                      success: function(answer) {

                                  if(answer.length>0){
                                  table_show.clear();
                                  listar();
                                    swal("Estudiante Ingresado Correctamente","", "success");
                                }else{
                                    swal("Ha ocurrido un error.","Inténtelo nuevamente", "error");
                                }
                          }

                    });



}

///////////////////////DATATABLE
var table_show =  $('#table1').DataTable({

  "deferRender": true,
   responsive: true,
   "scrollX": true,
  "language": {

  "sProcessing":     "Procesando...",
  "sLengthMenu":     "Mostrar _MENU_ registros",
  "sZeroRecords":    "No se encontraron resultados",
  "sEmptyTable":     "Ningún dato disponible en esta tabla",
  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
  "sInfoPostFix":    "",
  "sSearch":         "Buscar:",
  "sUrl":            "",
  "sInfoThousands":  ",",
  "sLoadingRecords": "Cargando...",
  "oPaginate": {
      "sFirst":    "Primero",
      "sLast":     "Último",
      "sNext":     "Siguiente",
      "sPrevious": "Anterior"
  },
  "oAria": {
      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  }
}
});

listar();

function listar(){

        $.ajax({
          url: '/cargarPersonal2',
          success: function(answer) {

                    for (var i = answer.length - 1; i >= 0; i--)
                    {
                        var titulos='';
                        for (var j = 0;  j < answer[i].comentarios.length; j++) {
                          titulos+=answer[i].comentarios[j].NOMBRE_TITULO+' ';

                        }

                        var rowNode = table_show
                        .row.add([
                          '<button  data-toggle="modal" data-target="#actualizarDatos" type="button" class="editar btn btn-primary btn-sm"><i class="fas fa-edit"></i> </button> '+
                          '<button data-toggle="modal" data-target="#modalCambioContrasenia" type="button" class="eliminar btn btn-danger  btn-sm" id="eliminarRegistro1"><i class="fas fa-key"></i></button> ',
                          //'<button  type="button" class="agregar btn btn-success  btn-sm" id="eliminarRegistro1"><i class="fas fa-plus-circle"></i></button>',
                                  answer[i].CODIGO_MAESTRO,
                                  titulos,
                                  answer[i].NOMBRE,
                                  answer[i].APELLIDO,
                                  answer[i].NOMBRE_GENERO,
                                  answer[i].NOMBRE_CARGO,
                                  answer[i].SUELDO_BASE,
                                  answer[i].LUGAR_NACIMIENTO,
                                  answer[i].LUGAR_RESIDENCIA,
                                  answer[i].MUNICIPIO_NACIMIENTO,
                                  answer[i].MUNICIPIO_RESIDENCIA,
                                  answer[i].NOMBRE_CAMPUS,
                                  answer[i].TIPO_IDENTIFICACION,
                                  answer[i].CORREO_ELECTRONICO,
                                  answer[i].DIRECCION,
                                  answer[i].FECHA_NACIMIENTO,
                                  answer[i].IDENTIDAD,
                                  answer[i].TELEFONO,
                                  answer[i].NOMBRE_TIPO_EMPLEADO,
                                  answer[i].DESCRIPCION,
                                  answer[i].NOMBRE_TITULARIDAD,
                                  answer[i].SALARIO_MAXIMO,
                                  answer[i].SALARIO_MINIMO
                                ])

                        .draw()
                        .node();
                    }

                  }

        });

      obtenerData("#table1 tbody",table_show);
      obtenerDataDelete("#table1 tbody",table_show);
}


function obtenerData(tbody, table){
  $(tbody).on("click","button.editar",function(){
    var data = table.row($(this).parents("tr")).data();
    codigoPersona=data[1];
    $("#telefonoPersona1").val(data[18]);
    $("#correoPersona1").val(data[14]);
    $("#direccionPersona1").val(data[15]);
    $("#lugarResidenciaPersona1 option:contains("+data[9]+")").attr('selected', true);
    $("#tipoEmpleado1 option:contains("+data[19]+")").attr('selected', true);
    //console.log($("#lugarResidenciaPersona1").val());
    var parametros = 'codigoLugar='+$("#lugarResidenciaPersona1").val();
    $.ajax({
      url: '/slcMunicipio',
      method:'POST',
      data:parametros,
      success: function(answer) {

                var contenido='<option value="0" selected disabled>Municipio de Residencia</option>';
                for (var i = answer.length - 1; i >= 0; i--)
                {
                    contenido+='<option value="'+answer[i].CODIGO_LUGAR+'">'+answer[i].NOMBRE_LUGAR+'</option>'
                }

                $("#municipioResidenciaPersona1").html(contenido);
                $("#municipioResidenciaPersona1 option:contains("+data[11]+")").attr('selected', true);

              }
    });
  });
}



function obtenerDataDelete(tbody, table){
  $(tbody).on("click","button.eliminar",function(){
    var data = table.row($(this).parents("tr")).data();
    codigoPersona=data[1];
  //  console.log(codigoPersona);

  });
}

$("#accionCambiarPassword").click(function(){
  if($("#CambiocontraseñaNueva").val()=='' || (/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test($("#CambiocontraseñaNueva").val()))!=true){
    $("#CambiocontraseñaNueva").addClass('is-invalid');
  }else{
    $("#CambiocontraseñaNueva").removeClass('is-invalid');
    var parametro="newPassword="+$("#CambiocontraseñaNueva").val()+'&codPersona='+codigoPersona;
    $.ajax({
      url: '/cambioContrasenia1',
      method:'POST',
      data:parametro,
      success: function(answer) {

            swal("Contraseña cambiada con éxito.", "", "success");
            $("#CambiocontraseñaNueva").val('');

      }
    });
  }
});


//////////////////////VALIDACIONES

    $("#enviarPersonal").click(function(){
      if ($("#nombresPersona").val()=='' || (/^([a-z ñáéíóú]{4,60})$/i.test($("#nombresEstudiante").val()))!=true) {
        $("#nombresPersona").addClass('is-invalid');
      }else if ($("#apellidosPersona").val()=='' || (/^([a-z ñáéíóú]{4,60})$/i.test($("#apellidosPersona").val()))!=true) {
        $("#apellidosPersona").addClass('is-invalid');
        $("#nombresPersona").removeClass('is-invalid');
      }else if ($("#idPersona").val()=='' || (/^[0-9][0-9][0-9][0-9]-\d{4}-\d{5}$/.test($("#idPersona").val()))!=true) {
        $("#idPersona").addClass('is-invalid');
        $("#apellidosPersona").removeClass('is-invalid');
      }else if ($("#fechaNacimientoPersona").val()=='') {
        $("#fechaNacimientoPersona").addClass('is-invalid');
        $("#idPersona").removeClass('is-invalid');
      }else if ($("#telefonoPersona").val()=='' || (/^[2-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/.test($("#telefonoPersona").val()))!=true) {
        $("#telefonoPersona").addClass('is-invalid');
        $("#fechaNacimientoPersona").removeClass('is-invalid');
      }else if ($("#correoPersona").val()=='' || ((/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($("#correoPersona").val()))!=true)) {
        $("#correoPersona").addClass('is-invalid');
        $("#telefonoPersona").removeClass('is-invalid');
      }else if ($("#generoPersona").val()==0 || $("#generoPersona").val()==null) {
        $("#generoPersona").addClass('is-invalid');
        $("#correoPersona").removeClass('is-invalid');
      }else if ($("#estadoCivilPersona").val()==0 || $("#estadoCivilPersona").val()==null) {
        $("#estadoCivilPersona").addClass('is-invalid');
        $("#generoPersona").removeClass('is-invalid');
      }else if ($("#documentacionPersona").val()==0 || $("#documentacionPersona").val()==null) {
        $("#documentacionPersona").addClass('is-invalid');
        $("#estadoCivilPersona").removeClass('is-invalid');
      }else if ($("#campusPersona").val()==0 || $("#campusPersona").val()==null) {
        $("#campusPersona").addClass('is-invalid');
        $("#documentacionPersona").removeClass('is-invalid');
      }else if ($("#direccionPersona").val()=='') {
        $("#direccionPersona").addClass('is-invalid');
        $("#campusPersona").removeClass('is-invalid');
      }else if ($("#lugarResidenciaPersona").val()==0 || $("#lugarResidenciaPersona").val()==null) {
        $("#lugarResidenciaPersona").addClass('is-invalid');
        $("#direccionPersona").removeClass('is-invalid');
      }else if ($("#municipioResidenciaPersona").val()==0 || $("#municipioResidenciaPersona").val()==null) {
        $("#municipioResidenciaPersona").addClass('is-invalid');
        $("#lugarResidenciaPersona").removeClass('is-invalid');
      }else if ($("#lugarNacimientoPersona").val()==0 || $("#lugarNacimientoPersona").val()==null) {
        $("#lugarNacimientoPersona").addClass('is-invalid');
        $("#municipioResidenciaPersona").removeClass('is-invalid');
      }else if ($("#municipioNacimientoPersona").val()==0 || $("#municipioNacimientoPersona").val()==null) {
        $("#municipioNacimientoPersona").addClass('is-invalid');
        $("#lugarNacimientoPersona").removeClass('is-invalid');
      }else if ($("#numeroEmpleado").val()=='') {
        $("#numeroEmpleado").addClass('is-invalid');
        $("#municipioNacimientoPersona").removeClass('is.invalid');
      }else if ($("#contrasenia").val()=='') {
        $("#contrasenia").addClass('is-invalid');
        $("#numeroEmpleado").removeClass('is-invalid');
      }else if ($("#saldoBase").val()=='') {
        $("#saldoBase").addClass('is-invalid');
        $("#contrasenia").removeClass('is-invalid');
      }else if ($("#codigoTitulo").val()==0 || $("#codigoTitulo").val()==null) {
        $("#codigoTitulo").addClass('is-invalid');
        $("#saldoBase").removeClass('is-invalid');
      }else if ($("#codigoUniversidad").val()==0 || $("#codigoUniversidad").val()==null) {
        $("#codigoUniversidad").addClass('is-invalid');
        $("#codigoTitulo").removeClass('is-invalid');
      }else if ($("#fechaTitulo").val()=='') {
        $("#fechaTitulo").addClass('is-invalid');
        $("#codigoUniversidad").removeClass('is-invalid');
      }else if ($("#numeroRegistro").val()=='') {
        $("#numeroRegistro").addClass('is-invalid');
        $("#fechaTitulo").removeClass('is-invalid');
      }else if ($("#cargoPesonal").val()==0 || $("#cargoPesonal").val()==null){
        $("#cargoPesonal").addClass('is-invalid');
        $("#numeroRegistro").removeClass('is-invalid');
      }else if ($("#tipoEmpleado").val()==0 || $("#tipoEmpleado").val()==null){
        $("#tipoEmpleado").addClass('is-invalid');
        $("#cargoPesonal").removeClass('is-invalid');
      }else if ($("#titularidadDocente").val()==0 || $("#titularidadDocente").val()==null){
        $("#titularidadDocente").addClass('is-invalid');
        $("#tipoEmpleado").removeClass('is-invalid');
      }else if ($("#especializacionDocente").val()==0 || $("#especializacionDocente").val()==null){
        $("#especializacionDocente").addClass('is-invalid');
        $("#titularidadDocente").removeClass('is-invalid');
      }else{
        $("#especializacionDocente").removeClass('is-invalid');
        //alert('ok');
        guardarPersonal();
      }

    });

    $("#actualizarDatosPersona").click(function(){

      if ($("#telefonoPersona1").val()=='' || (/^[2-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/.test($("#telefonoPersona1").val()))!=true) {
        $("#telefonoPersona1").addClass('is-invalid');
      }else if ($("#correoPersona1").val()=='' || ((/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($("#correoPersona1").val()))!=true)) {
        $("#correoPersona1").addClass('is-invalid');
        $("#telefonoPersona1").removeClass('is-invalid');
      }else if ($("#direccionPersona1").val()=='') {
        $("#direccionPersona1").addClass('is-invalid');
        $("#correoPersona1").removeClass('is-invalid');
      }else if ($("#lugarResidenciaPersona1").val()==0 || $("#lugarResidenciaPersona1").val()==null) {
        $("#lugarResidenciaPersona1").addClass('is-invalid');
        $("#direccionPersona1").removeClass('is-invalid');
      }else if ($("#municipioResidenciaPersona1").val()==0 || $("#municipioResidenciaPersona1").val()==null) {
        $("#municipioResidenciaPersona1").addClass('is-invalid');
        $("#lugarResidenciaPersona1").removeClass('is-invalid');
      }else if ($("#tipoEmpleado1").val()==0 || $("#tipoEmpleado1").val()==null) {
        $("#tipoEmpleado1").addClass('is-invalid');
        $("#municipioResidenciaPersona1").removeClass('is-invalid');
      }else{
        $("#tipoEmpleado1").removeClass('is-invalid');

        var parametro="codigoPersona="+codigoPersona+"&telefonoPersona="+$("#telefonoPersona1").val()+
                      "&correoPersona="+$("#correoPersona1").val()+
                      "&direccionPersona="+$("#direccionPersona1").val()+
                      "&resPersona="+$("#lugarResidenciaPersona1").val()+
                      "&munPersona="+$("#municipioResidenciaPersona1").val()+
                      "&tipoEmpleado="+$("#tipoEmpleado1").val();
        $.ajax({
          url: '/actualizarDatosPersona1',
          method:'POST',
          data:parametro,
          success: function(answer) {
                  table_show.clear();
                  listar();
                swal("Cambios realizados con éxito.", "", "success");
                $("#CambiocontraseñaNueva").val('');

          }
        });
      }

    });


});
