

$(document).ready(function(){

    var codigoPeriodo;
    $.ajax({
      url: '/slcTipoPeriodo',
      success: function(answer) {

                var contenido='<option value="0" selected disabled>Tipo Período*</option>';
                for (var i = answer.length - 1; i >= 0; i--)
                {
                    contenido+='<option value="'+answer[i].CODIGO_TIPO_PERIODO+'">'+answer[i].TIPO_PERIODO+'</option>'
                }
                $("#slcTipoPeriodo").html(contenido);

              }

    });

    ////////////////////////////////FUNCIONES
    $("#enviarPeriodo").click(function(){
        if ($("#numeroPeriodo").val()=='') {
          $("#numeroPeriodo").addClass('is-invalid');
        }else if ($("#fechaInicioPeriodo").val()=='') {
          $("#numeroPeriodo").removeClass('is-invalid');
          $("#fechaInicioPeriodo").addClass('is-invalid');
        }else if ($("#fechaFinalPeriodo").val()=='') {
          $("#fechaInicioPeriodo").removeClass('is-invalid');
          $("#fechaFinalPeriodo").addClass('is-invalid');
        }else if ($("#slcTipoPeriodo").val()=='' || $("#slcTipoPeriodo").val()==null) {
          $("#slcTipoPeriodo").addClass('is-invalid');
          $("#fechaFinalPeriodo").removeClass('is-invalid');
        }else{
              guardarPeriodo();
              $("#slcTipoPeriodo").removeClass('is-invalid');
              $("#codigoPeriodo").val('');
              $("#numeroPeriodo").val('');
              $("#fechaInicioPeriodo").val('');
              $("#fechaFinalPeriodo").val('');
              $("#anioPeriodo").val('');
              table_show.clear();
              listar();

        }
    });


    $("#actualizarPeriodo").click(function(){
        if ($("#numeroPeriodo").val()=='') {
          $("#numeroPeriodo").addClass('is-invalid');
        }else if ($("#fechaInicioPeriodo").val()=='') {
          $("#numeroPeriodo").removeClass('is-invalid');
          $("#fechaInicioPeriodo").addClass('is-invalid');
        }else if ($("#fechaFinalPeriodo").val()=='') {
          $("#fechaInicioPeriodo").removeClass('is-invalid');
          $("#fechaFinalPeriodo").addClass('is-invalid');
        }else if ($("#slcTipoPeriodo").val()=='' || $("#slcTipoPeriodo").val()==null) {
          $("#slcTipoPeriodo").addClass('is-invalid');
          $("#fechaFinalPeriodo").removeClass('is-invalid');
        }else{
              actualizarPeriodo();
              $("#slcTipoPeriodo").removeClass('is-invalid');
              $("#codigoPeriodo").val('');
              $("#numeroPeriodo").val('');
              $("#fechaInicioPeriodo").val('');
              $("#fechaFinalPeriodo").val('');
              $("#anioPeriodo").val('');
              table_show.clear();
              listar();

        }
    });



    function guardarPeriodo(){
      var parametros = 'nombrePeriodo='+$("#numeroPeriodo").val()+'&fechaInicio='+$("#fechaInicioPeriodo").val()+
                        '&fechaFin='+$("#fechaFinalPeriodo").val()+'&tipoPeriodo='+$("#slcTipoPeriodo").val();

      $.ajax({
            url:"/guardarPeriodo",
            method:'POST',
            data:parametros,
            success:function(respuesta){

                if (respuesta=='correcto') {
                  swal("Acción realizada con éxito!", "", "success");
                }else {
                    swal("Acción no se realizó con éxito!", "", "error");
                }

            }
            });
    }

    function actualizarPeriodo(){
      var parametros = 'nombrePeriodo='+$("#numeroPeriodo").val()+'&fechaInicio='+$("#fechaInicioPeriodo").val()+
                        '&fechaFin='+$("#fechaFinalPeriodo").val()+'&tipoPeriodo='+$("#slcTipoPeriodo").val()+'&codigoPeriodo='+codigoPeriodo;

      $.ajax({
            url:"/actualizarPeriodo",
            method:'POST',
            data:parametros,
            success:function(respuesta){

                if (respuesta=='correcto') {
                  swal("Acción realizada con éxito!", "", "success");
                }else {
                    swal("Acción no se realizó con éxito!", "", "error");
                }

            }
            });
    }



function cambiar(){
    $("#actualizarPeriodo").attr('hidden',false);
    $("#enviarPeriodo").attr('hidden',true);
}

$("#cerrarModalPeriodo").click(function(){
  $("#enviarPeriodo").removeAttr('hidden');
  $("#actualizarPeriodo").attr('hidden',true);

});



    ////////////////////////DataTable

    var table_show =  $('#table1').DataTable({

       responsive: true,
      "language": {

      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
  });

  listar();

  function listar(){

          $.ajax({
            url: '/cargarPeriodos',
            success: function(answer) {

                      for (var i = answer.length - 1; i >= 0; i--)
                      {
                          var rowNode = table_show
                          .row.add([
                                      answer[i].CODIGO_PERIODO,
                                      answer[i].NOMBRE_PERIODO,
                                      answer[i].FECHA_INICIO,
                                      answer[i].FECHA_FIN,
                                      answer[i].TIPO_PERIODO,
                                      '<button  data-toggle="modal" data-target="#adicionarCarrera" type="button" class="editar btn btn-primary btn-sm"><i class="fas fa-edit"></i> </button> '
                                  ])
                          .draw()
                          .node();
                      }

                    }

          });

        obtenerData("#table1 tbody",table_show);

  }

  function obtenerData(tbody, table){
    $(tbody).on("click","button.editar",function(){
      var data = table.row($(this).parents("tr")).data();
      codigoPeriodo=data[0];
      $("#numeroPeriodo").val(data[1]);
      $("#fechaInicioPeriodo").val(data[2]);
      $("#fechaFinalPeriodo").val(data[3]);
      $("#slcTipoPeriodo option:contains("+data[4]+")").attr('selected', true);
      $("#actualizarPeriodo").attr('hidden',false);
      $("#enviarPeriodo").attr('hidden',true);
    });
  }




});
