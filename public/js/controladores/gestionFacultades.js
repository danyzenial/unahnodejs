


  var idFacultad;
  var table_show =  $('#table1').DataTable({

    /*"ajax": {"url":"../PHP/responses.php",
    "dataSrc": ""},
    "columns":[
      {"data":"NOMBRE_EMPLEADO"},
      {"data":"Apellido_EMPLEADO"},
      {"data":"NOMBRE_ROL"},
      {"data":"NOMBRE_USUARIO"},
      {"data":"TELEFONO_EMPLEADO"},
      {"data":"CORREO_EMPLEADO"},
      {"defaultContent":'<button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#editarUsuarioModal" class="btn btn-success"> Editar</button><button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#asignarTopicoModal" class="btn btn-primary"> Asignar Tópico</button><button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#infoUsuarioModal" class="btn btn-warning"> Info</button>' }
    ],*/
     responsive: true,
    "language": {

    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
  }
});

listar();

function listar(){

        $.ajax({
          url: '/cargarFacultades',
          success: function(answer) {

                    for (var i = answer.length - 1; i >= 0; i--)
                    {
                        var rowNode = table_show
                        .row.add([
                                    answer[i].CODIGO_FACULTAD,
                                    answer[i].NOMBRE_FACULTAD,
                                    answer[i].DESCRIPCION,
                                    '<button  data-toggle="modal" data-target="#actualizarFacultad" type="button" class="editar btn btn-primary btn-sm"><i class="fas fa-edit"></i> </button> '+
                                    '<button data-toggle="modal" data-target="#confirmarRespuesta" type="button" class="eliminar btn btn-danger  btn-sm" id="eliminarRegistro1"><i class="fas fa-trash-alt"></i></button>'
                                ])
                        .draw()
                        .node();
                    }

                  }

        });

      obtenerData("#table1 tbody",table_show);
      obtenerDataDelete("#table1 tbody",table_show);

}




    $("#modal-btn-si").on("click", function(){
      $("#confirmarRespuesta").modal('hide');
      var parametros='idFacultad='+idFacultad;
      $.ajax({
        url: '/eliminarFacultad',
        method:'POST',
        data:parametros,
        success: function(answer) {
              table_show.clear();
              listar();
            }

      });

    });

    $("#modal-btn-no").on("click", function(){
      $("#confirmarRespuesta").modal('hide');
    });




function obtenerData(tbody, table){
  $(tbody).on("click","button.editar",function(){
    var data = table.row($(this).parents("tr")).data();
    console.log(data);
    $("#nombreFacultad1").val(data[1]);
    $("#descripcionFacultad1").val(data[2]);
    idFacultad=data[0];
  });
}

function obtenerDataDelete(tbody, table){
  $(tbody).on("click","button.eliminar",function(){
    var data = table.row($(this).parents("tr")).data();
    console.log(data[0]);
    idFacultad=data[0];
  });
}

$("#actualizar").click(function(){

  var parametros='nombreFacultad1='+$("#nombreFacultad1").val()+'&descripcionFacultad1='+$("#descripcionFacultad1").val()+"&idFacultad="+idFacultad;

  $.ajax({
    url: '/actualizarFacultades',
    method:'POST',
    data:parametros,
    success: function(answer) {


        }
  })

  table_show.clear();
  listar();

});




$("#enviarFacultad" ).click(function() {

  if($("#nombreFacultad").val()=='' || $("#descripcionFacultad").val()==''){
    //alert("Campos Obligatorios");
    $("#nombreFacultad").addClass('is-invalid');
    $("#descripcionFacultad").addClass('is-invalid');
  }else{
    guardarFacultad();
    table_show.clear();
    listar();
    $("#nombreFacultad").removeClass('is-invalid');
    $("#descripcionFacultad").removeClass('is-invalid');
    $("#nombreFacultad").val('');
    $("#descripcionFacultad").val('');

  }
});

function guardarFacultad(){
  var parametros = 'nombreFacultad='+$("#nombreFacultad").val()+'&descripcionFacultad='+$("#descripcionFacultad").val();
  $.ajax({
        url:"/guardarFacultad",
        method:'POST',
        data:parametros,
        success:function(respuesta){
          table_show.clear();
          listar();
              console.log(respuesta);

        }
        });
}
