

$( document ).ready(function() {
    console.log( "ready!" );
});


$( "#ingresarEstudiante" ).click(function() {



  if($("#numeroCuenta").val()=='' || $("#contraseñaEstudiante").val()==''){
    //alert("Campos Obligatorios");

    swal("Atención Estudiante", "Número de Cuenta y Contraseña Obligatorios", "warning")

  }else{
    var parametros='nCuenta='+$("#numeroCuenta").val()+'&password='+$("#contraseñaEstudiante").val();
    $.ajax({
      url:'/loginEstudiante',
      data:parametros,
      method:'POST',
      success: function(respuesta){
        if (respuesta.estatus ==0 ){
          window.location.href ="menuEstudiante.html";
      }else{
        swal("Número de cuenta o contraseña Incorrectos", "Verifique y vuelva a intentar", "error")}
      }

    });
  }
});
