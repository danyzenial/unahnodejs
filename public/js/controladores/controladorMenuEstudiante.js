


$.ajax({
  url:'/obtenerDatos',
  method:'POST',
  success: function(respuesta){

        $("#nombreUsuario").html('<h4>'+respuesta[0].DATOS+'</h4>');
  }
});




function verificarFechaMatricula(){

  $.ajax({
    url:'/verificarFechaMatricula',
    data:'fechaActual='+fechaActual(),
    method:'POST',
    success: function(respuesta){
        if(respuesta.respuesta=='no'){
          swal("Todavía no es tiempo de matrícula", "Revise el calendario en la página principal", "warning")
        }else{
          window.location.href='matriculaEstudiante.html';
        }
    }
  });


}



function verificarVerCalificaciones(){

  $.ajax({
    url:'/verificarVerCalificaciones',
    data:'fechaActual='+fechaActual(),
    method:'POST',
    success: function(respuesta){
        if(respuesta.respuesta=='no'){
          swal("Todavía no es tiempo de matrícula", "Revise el calendario en la página principal", "warning")
        }else{
          window.location.href='calificacionesPeriodo.html';
        }
    }
  });


}

function verificarEvaluarDocente(){

  $.ajax({
    url:'/verificarFechaMatricula',
    data:'fechaActual='+fechaActual(),
    method:'POST',
    success: function(respuesta){
        if(respuesta.respuesta=='no'){
          swal("Calificaciones no se pueden ver", "Revise el calendario en la página principal", "warning")
        }else{
          window.location.href='calificacionesPeriodo.html';
        }
    }
  });

}

$("#cerrarSession").click(function(){
    $.ajax({
      url:'/logout',
      success: function(){
            window.location.href='index.html';
        }
      });
});


function fechaActual(){
      var d = new Date();

      var month = d.getMonth()+1;
      var day = d.getDate();

      var output = d.getFullYear() + '-' +
          ((''+month).length<2 ? '0' : '') + month + '-' +
          ((''+day).length<2 ? '0' : '') + day;

      return output
}
