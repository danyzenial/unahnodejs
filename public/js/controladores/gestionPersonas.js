
$(document).ready(function (){

  var estadoCuenta, codigoPersona;

    $.ajax({
      url:'/slcGenero',
      success: function(answer){
        var contenido='<option value="0" selected disabled>Género*</option>';
        for (var i = answer.length - 1; i >= 0; i--)
        {
            contenido+='<option value="'+answer[i].CODIGO_GENERO+'">'+answer[i].NOMBRE_GENERO+'</option>'
        }
        $("#generoPersona").html(contenido);

      }

    });

    $.ajax({
      url:'/slcCivil',
      success: function(answer){
        var contenido='<option value="0" selected disabled>Estado Civil*</option>';
        for (var i = answer.length - 1; i >= 0; i--)
        {
            contenido+='<option value="'+answer[i].CODIGO_ESTADO_CIVIL+'">'+answer[i].NOMBRE_ESTADO_CIVIL+'</option>'
        }
        $("#estadoCivilPersona").html(contenido);

      }

    });

    $.ajax({
      url:'/slcIdentificacion',
      success: function(answer){
        var contenido='<option value="0" selected disabled>Tipo de Identificación*</option>';
        for (var i = answer.length - 1; i >= 0; i--)
        {
            contenido+='<option value="'+answer[i].CODIGO_TIPO_IDENTIFICACION+'">'+answer[i].TIPO_IDENTIFICACION+'</option>'
        }
        $("#documentacionPersona").html(contenido);

      }

    });

    $.ajax({
      url:'/slcCampus',
      success: function(answer){
        var contenido='<option value="0" selected disabled>Campus*</option>';
        for (var i = answer.length - 1; i >= 0; i--)
        {
            contenido+='<option value="'+answer[i].CODIGO_CAMPUS+'">'+answer[i].NOMBRE_CAMPUS+'</option>'
        }
        $("#campusPersona").html(contenido);

      }

    });

    $.ajax({
      url:'/slcLugar',
      success: function(answer){
        var contenido;
        for (var i = answer.length - 1; i >= 0; i--)
        {
            contenido+='<option value="'+answer[i].CODIGO_LUGAR+'">'+answer[i].NOMBRE_LUGAR+'</option>'
        }
        $("#lugarNacimientoPersona").append(contenido);
        $("#lugarResidenciaPersona").append(contenido);
        $("#lugarResidenciaPersona1").append(contenido);


      }

    });

    $.ajax({
      url:'/slcCarreras',
      success: function(answer){
        var contenido='<option value="0" selected disabled>Carrera*</option>';
        for (var i = answer.length - 1; i >= 0; i--)
        {
            contenido+='<option value="'+answer[i].CODIGO_CARRERA+'">'+answer[i].NOMBRE_CARRERA+'</option>'
        }
        $("#carreraEstudiante").html(contenido);



      }

    });

    $("#lugarNacimientoPersona").change(function(){
      var parametros = 'codigoLugar='+$("#lugarNacimientoPersona").val();
      $.ajax({
        url: '/slcMunicipio',
        method:'POST',
        data:parametros,
        success: function(answer) {

                  var contenido='<option value="0" selected disabled>Municipio de Nacimiento</option>';
                  for (var i = answer.length - 1; i >= 0; i--)
                  {
                      contenido+='<option value="'+answer[i].CODIGO_LUGAR+'">'+answer[i].NOMBRE_LUGAR+'</option>'
                  }
                  $("#municipioNacimientoPersona").html(contenido);

                }

      });
    });

    $("#lugarResidenciaPersona").change(function(){
      var parametros = 'codigoLugar='+$("#lugarResidenciaPersona").val();
      $.ajax({
        url: '/slcMunicipio',
        method:'POST',
        data:parametros,
        success: function(answer) {

                  var contenido='<option value="0" selected disabled>Municipio de Residencia</option>';
                  for (var i = answer.length - 1; i >= 0; i--)
                  {
                      contenido+='<option value="'+answer[i].CODIGO_LUGAR+'">'+answer[i].NOMBRE_LUGAR+'</option>'
                  }
                  $("#municipioResidenciaPersona").html(contenido);

                }

      });
    });

    $("#lugarResidenciaPersona1").change(function(){
      var parametros = 'codigoLugar='+$("#lugarResidenciaPersona1").val();
      $.ajax({
        url: '/slcMunicipio',
        method:'POST',
        data:parametros,
        success: function(answer) {

                  var contenido='<option value="0" selected disabled>Municipio de Residencia</option>';
                  for (var i = answer.length - 1; i >= 0; i--)
                  {
                      contenido+='<option value="'+answer[i].CODIGO_LUGAR+'">'+answer[i].NOMBRE_LUGAR+'</option>'
                  }
                  $("#municipioResidenciaPersona1").html(contenido);

                }

      });
    });




    function guardarEstudiante(){
      var estudiante =  'codigoGenero='+$("#generoPersona").val()+
                        '&lugarNacimiento='+$("#lugarNacimientoPersona").val()+
                        '&lugarResidencia='+$("#lugarResidenciaPersona").val()+
                        '&municioNacimiento='+$("#municipioNacimientoPersona").val()+
                        '&municioResidencia='+$("#municipioResidenciaPersona").val()+
                        '&campus='+$("#campusPersona").val()+
                        '&identificacion='+$("#documentacionPersona").val()+
                        '&estadoCivil='+$("#estadoCivilPersona").val()+
                        '&nombre='+$("#nombresPersona").val()+
                        '&apellido='+$("#apellidosPersona").val()+
                        '&identidad='+$("#idPersona").val()+
                        '&direccion='+$("#direccionPersona").val()+
                        '&telefono='+$("#telefonoPersona").val()+
                        '&email='+$("#correoPersona").val()+
                        '&carrera='+$("#carreraEstudiante").val()+
                        '&numeroCuenta='+$("#numeroCuenta").val()+
                        '&paa='+$("#paaEstudiante").val()+
                        '&contrasenia='+$("#contrasenia").val();



                        $.ajax({
                          url: '/guardarEstudiante',
                          method:'POST',
                          data:estudiante,
                          success: function(answer) {

                                      if(answer.length>0){
                                      table_show.clear();
                                      listar();
                                        swal("Estudiante Ingresado Correctamente","", "success");
                                    }else{
                                        swal("Ha ocurrido un error.","Inténtelo nuevamente", "error");
                                    }
                              }

                        });



    }

   $("#numeroCuenta").keyup(function(){
      var parametros = 'nCuenta='+$("#numeroCuenta").val();

      $.ajax({
        url: '/verificarCuenta',
        method:'POST',
        data:parametros,
        success: function(answer) {
            if(answer.length>0){
              swal("Número de cuenta ya Existe!", "Ingreselo correctamente", "error");
              }
        }
      });

    });


    $("#accionCambiarPassword").click(function(){
      if($("#CambiocontraseñaNueva").val()=='' || (/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test($("#CambiocontraseñaNueva").val()))!=true){
        $("#CambiocontraseñaNueva").addClass('is-invalid');
      }else{
        $("#CambiocontraseñaNueva").removeClass('is-invalid');
        var parametro="newPassword="+$("#CambiocontraseñaNueva").val()+'&codAlumno='+codigoPersona;
        $.ajax({
          url: '/cambioContrasenia',
          method:'POST',
          data:parametro,
          success: function(answer) {

                swal("Contraseña cambiada con éxito.", "", "success");
                $("#CambiocontraseñaNueva").val('');

          }
        });
      }
    });


    $("#actualizarDatosPersona").click(function(){

      if ($("#telefonoPersona1").val()=='' || (/^[2-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/.test($("#telefonoPersona1").val()))!=true) {
        $("#telefonoPersona1").addClass('is-invalid');
      }else if ($("#correoPersona1").val()=='' || ((/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($("#correoPersona1").val()))!=true)) {
        $("#correoPersona1").addClass('is-invalid');
        $("#telefonoPersona1").removeClass('is-invalid');
      }else if ($("#direccionPersona1").val()=='') {
        $("#direccionPersona1").addClass('is-invalid');
        $("#correoPersona1").removeClass('is-invalid');
      }else if ($("#lugarResidenciaPersona1").val()==0 || $("#lugarResidenciaPersona1").val()==null) {
        $("#lugarResidenciaPersona1").addClass('is-invalid');
        $("#direccionPersona1").removeClass('is-invalid');
      }else if ($("#municipioResidenciaPersona1").val()==0 || $("#municipioResidenciaPersona1").val()==null) {
        $("#municipioResidenciaPersona1").addClass('is-invalid');
        $("#lugarResidenciaPersona1").removeClass('is-invalid');
      }else{
          $("#municipioResidenciaPersona1").removeClass('is-invalid');
          $("#CambiocontraseñaNueva").removeClass('is-invalid');

          var parametro="codigoPersona="+codigoPersona+"&telefonoPersona="+$("#telefonoPersona1").val()+
                        "&correoPersona="+$("#correoPersona1").val()+
                        "&direccionPersona="+$("#direccionPersona1").val()+
                        "&resPersona="+$("#lugarResidenciaPersona1").val()+
                        "&munPersona="+$("#municipioResidenciaPersona1").val();
          $.ajax({
            url: '/actualizarDatosPersona',
            method:'POST',
            data:parametro,
            success: function(answer) {
                    table_show.clear();
                    listar();
                  swal("Cambios realizados con éxito.", "", "success");
                  $("#CambiocontraseñaNueva").val('');

            }
          });
      }

    });

    ///////////////////////////////////////////////

    var table_show =  $('#table1').DataTable({

      "deferRender": true,
       responsive: true,
       "scrollX": true,
       buttons: [
        'print'
    ],
      buttons: [
          'print'
      ],
      "language": {

      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
  });

  listar();
  table_show.columns.adjust().draw();

  function listar(){

          $.ajax({
            url: '/cargarEstudiantes',
            success: function(answer) {

                      for (var i = answer.length - 1; i >= 0; i--)
                      {
                          var carreras='';
                          for (var j = 0;  j < answer[i].comentarios.length; j++) {
                            carreras+=answer[i].comentarios[j].NOMBRE_CARRERA+' ';

                          }

                          var rowNode = table_show
                          .row.add([
                            '<button  data-toggle="modal" data-target="#actualizarDatos" type="button" class="editar btn btn-primary btn-sm"><i class="fas fa-edit"></i> </button> '+
                            '<button data-toggle="modal" data-target="#modalCambioContrasenia" type="button" class="eliminar btn btn-danger  btn-sm" id="eliminarRegistro1"><i class="fas fa-key"></i></button>',
                                      answer[i].CODIGO_ALUMNO,
                                      answer[i].NUMERO_CUENTA,
                                      carreras,
                                      answer[i].PROMEDIO,
                                      answer[i].NOMBRE,
                                      answer[i].APELLIDO,
                                      answer[i].NOMBRE_GENERO,
                                      answer[i].LUGAR_NACIMIENTO,
                                      answer[i].MUNICIPIO_NACIMIENTO,
                                      answer[i].LUGAR_RESIDENCIA,
                                      answer[i].MUNICIPIO_RESIDENCIA,
                                      answer[i].NOMBRE_CAMPUS,
                                      answer[i].TIPO_IDENTIFICACION,
                                      answer[i].CORREO_ELECTRONICO,
                                      answer[i].DIRECCION,
                                      answer[i].FECHA_NACIMIENTO,
                                      answer[i].IDENTIDAD,
                                      answer[i].TELEFONO

                                  ])

                          .draw()
                          .node();
                      }

                    }

          });

        obtenerData("#table1 tbody",table_show);
        obtenerDataDelete("#table1 tbody",table_show);

  }

  function obtenerData(tbody, table){
    $(tbody).on("click","button.editar",function(){
      var data = table.row($(this).parents("tr")).data();
      codigoPersona=data[1];
      $("#telefonoPersona1").val(data[18]);
      $("#correoPersona1").val(data[14]);
      $("#direccionPersona1").val(data[15]);
      $("#lugarResidenciaPersona1 option:contains("+data[10]+")").attr('selected', true);
      console.log($("#lugarResidenciaPersona1").val());
      var parametros = 'codigoLugar='+$("#lugarResidenciaPersona1").val();
      $.ajax({
        url: '/slcMunicipio',
        method:'POST',
        data:parametros,
        success: function(answer) {

                  var contenido='<option value="0" selected disabled>Municipio de Residencia</option>';
                  for (var i = answer.length - 1; i >= 0; i--)
                  {
                      contenido+='<option value="'+answer[i].CODIGO_LUGAR+'">'+answer[i].NOMBRE_LUGAR+'</option>'
                  }

                  $("#municipioResidenciaPersona1").html(contenido);
                  $("#municipioResidenciaPersona1 option:contains("+data[11]+")").attr('selected', true);

                }

      });


    });



  }

  function obtenerDataDelete(tbody, table){
    $(tbody).on("click","button.eliminar",function(){
      var data = table.row($(this).parents("tr")).data();
      codigoPersona=data[1];
    });
  }

    /////////////////////////////////////////////


    $("#enviarPersona").click(function(){
      if ($("#nombresPersona").val()=='' || (/^([a-z ñáéíóú]{4,60})$/i.test($("#nombresEstudiante").val()))!=true) {
        $("#nombresPersona").addClass('is-invalid');
      }else if ($("#apellidosPersona").val()=='' || (/^([a-z ñáéíóú]{4,60})$/i.test($("#apellidosPersona").val()))!=true) {
        $("#apellidosPersona").addClass('is-invalid');
        $("#nombresPersona").removeClass('is-invalid');
      }else if ($("#idPersona").val()=='' || (/^[0-9][0-9][0-9][0-9]-\d{4}-\d{5}$/.test($("#idPersona").val()))!=true) {
        $("#idPersona").addClass('is-invalid');
        $("#apellidosPersona").removeClass('is-invalid');
      }else if ($("#fechaNacimientoPersona").val()=='') {
        $("#fechaNacimientoPersona").addClass('is-invalid');
        $("#idPersona").removeClass('is-invalid');
      }else if ($("#telefonoPersona").val()=='' || (/^[2-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/.test($("#telefonoPersona").val()))!=true) {
        $("#telefonoPersona").addClass('is-invalid');
        $("#fechaNacimientoPersona").removeClass('is-invalid');
      }else if ($("#correoPersona").val()=='' || ((/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($("#correoPersona").val()))!=true)) {
        $("#correoPersona").addClass('is-invalid');
        $("#telefonoPersona").removeClass('is-invalid');
      }else if ($("#generoPersona").val()==0 || $("#generoPersona").val()==null) {
        $("#generoPersona").addClass('is-invalid');
        $("#correoPersona").removeClass('is-invalid');
      }else if ($("#estadoCivilPersona").val()==0 || $("#estadoCivilPersona").val()==null) {
        $("#estadoCivilPersona").addClass('is-invalid');
        $("#generoPersona").removeClass('is-invalid');
      }else if ($("#documentacionPersona").val()==0 || $("#documentacionPersona").val()==null) {
        $("#documentacionPersona").addClass('is-invalid');
        $("#estadoCivilPersona").removeClass('is-invalid');
      }else if ($("#campusPersona").val()==0 || $("#campusPersona").val()==null) {
        $("#campusPersona").addClass('is-invalid');
        $("#documentacionPersona").removeClass('is-invalid');
      }else if ($("#direccionPersona").val()=='') {
        $("#direccionPersona").addClass('is-invalid');
        $("#campusPersona").removeClass('is-invalid');
      }else if ($("#lugarResidenciaPersona").val()==0 || $("#lugarResidenciaPersona").val()==null) {
        $("#lugarResidenciaPersona").addClass('is-invalid');
        $("#direccionPersona").removeClass('is-invalid');
      }else if ($("#municipioResidenciaPersona").val()==0 || $("#municipioResidenciaPersona").val()==null) {
        $("#municipioResidenciaPersona").addClass('is-invalid');
        $("#lugarResidenciaPersona").removeClass('is-invalid');
      }else if ($("#lugarNacimientoPersona").val()==0 || $("#lugarNacimientoPersona").val()==null) {
        $("#lugarNacimientoPersona").addClass('is-invalid');
        $("#municipioResidenciaPersona").removeClass('is-invalid');
      }else if ($("#municipioNacimientoPersona").val()==0 || $("#municipioNacimientoPersona").val()==null) {
        $("#municipioNacimientoPersona").addClass('is-invalid');
        $("#lugarNacimientoPersona").removeClass('is-invalid');
      }else if ($("#numeroCuenta").val()==''||(/^[1-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]?$/.test($("#numeroCuenta").val()))!=true) {
        $("#numeroCuenta").addClass('is-invalid');
        $("#municipioNacimientoPersona").removeClass('is-invalid');
      }else if ($("contrasenia").val()==0 || (/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test($("#contrasenia").val()))!=true) {
        $("#contrasenia").addClass('is-invalid');
        $("#numeroCuenta").removeClass('is-invalid');
      }else if ($("#carreraEstudiante").val()==0 || $("#carreraEstudiante").val()==null) {
        $("#carreraEstudiante").addClass('is-invalid');
        $("#contrasenia").removeClass('is-invalid');
      }else if ($("#paaEstudiante").val()=='') {
        $("#paaEstudiante").addClass('is-invalid');
        $("#carreraEstudiante").removeClass('is-invalid');
      }else{
        $("#paaEstudiante").removeClass('is-invalid');
        guardarEstudiante();
      }

    });

  });
