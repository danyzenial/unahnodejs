$(document).ready(function(){

  $( "#ingresarEmpleado" ).click(function() {



    if($("#numeroEmpleado").val()=='' || $("#contraseniaEmpleado").val()==''){
      //alert("Campos Obligatorios");

      swal("Atención", "Número de Empleado y Contraseña Obligatorios", "warning")

    }else{
      var parametros='nEmpleado='+$("#numeroEmpleado").val()+'&password='+$("#contraseniaEmpleado").val();
      $.ajax({
        url:'/loginEmpleado',
        data:parametros,
        method:'POST',
        success: function(respuesta){
          if (respuesta.estatus ==0 && respuesta.CODIGO_TIPO_EMPLEADO==3){
            window.location.href ="catedratico.html";
        }else if (respuesta.estatus ==0 && respuesta.CODIGO_TIPO_EMPLEADO==2) {
            window.location.href ="seapi.html";
        }else if (respuesta.estatus ==0 && respuesta.CODIGO_TIPO_EMPLEADO==1) {
            window.location.href ="administracion.html";
        }else if (respuesta.estatus ==0 && respuesta.CODIGO_TIPO_EMPLEADO==4) {
            window.location.href ="jefe.html";
        }else if (respuesta.estatus ==0 && respuesta.CODIGO_TIPO_EMPLEADO==5) {
            window.location.href ="dipp.html";
        }else{
          swal("Número de Empleado o contraseña Incorrectos", "Verifique que este en su departamento", "error")}
        }

      });
    }
  });

});
