

$(document).ready(function(){

  $.ajax({
    url: '/slcAsignatura',
    success: function(answer) {

              var contenido='<option value="0" selected disabled>Asignatura</option>';
              for (var i = answer.length - 1; i >= 0; i--)
              {
                  contenido+='<option value="'+answer[i].CODIGO_ASIGNATURA+'">'+answer[i].NOMBRE_ASIGNATURA+'</option>'
              }
              $("#slcAsignatura").html(contenido);

            }

  });

  $.ajax({
    url: '/slcAulas2',
    success: function(answer) {

              var contenido='<option value="0" selected disabled>Aula</option>';
              for (var i = answer.length - 1; i >= 0; i--)
              {
                  contenido+='<option value="'+answer[i].CODIGO_AULA+'">'+answer[i].NUMERO_AULA+'</option>'
              }
              $("#slcAula").html(contenido);

            }

  });

  $.ajax({
    url: '/slcMaestro',
    success: function(answer) {

              var contenido='<option value="0" selected disabled>Maestro</option>';
              for (var i = answer.length - 1; i >= 0; i--)
              {
                  contenido+='<option value="'+answer[i].CODIGO_MAESTRO+'">'+answer[i].MAESTRO+'</option>'
              }
              $("#slcMaestro").html(contenido);

            }

  });

  $.ajax({
    url: '/slcPeriodo',
    success: function(answer) {

              var contenido='<option value="0" selected disabled>Período</option>';
              for (var i = answer.length - 1; i >= 0; i--)
              {
                  contenido+='<option value="'+answer[i].CODIGO_PERIODO+'">'+answer[i].NOMBRE_PERIODO+'</option>'
              }
              $("#slcPeriodo").html(contenido);

            }

  });



});

var codigoSeccion;
var table_show =  $('#table1').DataTable({

  /*"ajax": {"url":"../PHP/responses.php",
  "dataSrc": ""},
  "columns":[
    {"data":"NOMBRE_EMPLEADO"},
    {"data":"Apellido_EMPLEADO"},
    {"data":"NOMBRE_ROL"},
    {"data":"NOMBRE_USUARIO"},
    {"data":"TELEFONO_EMPLEADO"},
    {"data":"CORREO_EMPLEADO"},
    {"defaultContent":'<button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#editarUsuarioModal" class="btn btn-success"> Editar</button><button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#asignarTopicoModal" class="btn btn-primary"> Asignar Tópico</button><button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#infoUsuarioModal" class="btn btn-warning"> Info</button>' }
  ],*/
   responsive: true,
  "language": {

  "sProcessing":     "Procesando...",
  "sLengthMenu":     "Mostrar _MENU_ registros",
  "sZeroRecords":    "No se encontraron resultados",
  "sEmptyTable":     "Ningún dato disponible en esta tabla",
  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
  "sInfoPostFix":    "",
  "sSearch":         "Buscar:",
  "sUrl":            "",
  "sInfoThousands":  ",",
  "sLoadingRecords": "Cargando...",
  "oPaginate": {
      "sFirst":    "Primero",
      "sLast":     "Último",
      "sNext":     "Siguiente",
      "sPrevious": "Anterior"
  },
  "oAria": {
      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  }
}
});


listar();

function listar(){

        $.ajax({
          url: '/cargarSecciones',
          success: function(answer) {

                    for (var i = answer.length - 1; i >= 0; i--)
                    {
                        var rowNode = table_show
                        .row.add([
                                    answer[i].CODIGO_SECCION,
                                    answer[i].DOCENTE,
                                    answer[i].NOMBRE_PERIODO,
                                    answer[i].NOMBRE_ASIGNATURA,
                                    answer[i].CANTIDAD_UNIDADES_VALORATIVAS,
                                    answer[i].CANTIDAD_CUPOS,
                                    '<button  data-toggle="modal" data-target="#agregarCupos" type="button" class="editar btn btn-primary btn-sm"><i class="fas fa-edit"></i> </button> '
                                ])
                        .draw()
                        .node();
                    }

                  }

        });

      obtenerData("#table1 tbody",table_show);


}

function obtenerData(tbody, table){
  $(tbody).on("click","button.editar",function(){
    var data = table.row($(this).parents("tr")).data();
    codigoSeccion=data[0];
  });
}

$("#enviarNuevosCupos").click(function(){
  var parametro='codigoSeccion='+codigoSeccion+'&cupos='+$("#cuposNuevos").val();
  $.ajax({
    url:'/actualizarSeccion',
    data:parametro,
    method:'POST',
    success: function(respuesta){
        if (respuesta!='') {
          table_show
          .clear()
          .draw();
          listar();
          swal("Acción realizada con éxito!", "", "success");
        } else {
          swal("Acción realizada sin éxito!", "Vuelva a intentar", "error");
        }
    }
  });
});

$("#enviarSeccion").click(function(){
  if($("#codigoAlterno").val()==''){
    $("#codigoAlterno").addClass('is-invalid');
  }else if ($("#horaInicio").val()=='') {
    $("#codigoAlterno").removeClass('is-invalid');
    $("#horaInicio").addClass('is-invalid');
  }else if ($("#horaFinal").val()=='') {
    $("#horaInicio").removeClass('is-invalid');
    $("#horaFinal").addClass('is-invalid');
  }else if ($("#cupos").val()=='' || (/^[0-6][0-9]?$/.test($("#cupos").val()))!=true) {
    $("#cupos").addClass('is-invalid');
    $("#horaFinal").removeClass('is-invalid');
  }else if ($("#slcAsignatura").val()==0 || $("#slcAsignatura").val()==null) {
    $("#slcAsignatura").addClass('is-invalid');
    $("#cupos").removeClass('is-invalid');
  }else if ($("#slcAula").val()==0 || $("#slcAula").val()==null) {
    $("#slcAula").addClass('is-invalid');
    $("#slcAsignatura").removeClass('is-invalid');
  }else if ($("#slcMaestro").val()==0 || $("#slcMaestro").val()==null) {
    $("#slcMaestro").addClass('is-invalid');
    $("#slcAula").removeClass('is-invalid');
  }else if ($("#slcDias").val()==0 || $("#slcDias").val()==null) {
    $("#slcDias").addClass('is-invalid');
    $("#slcMaestro").removeClass('is-invalid');
  }else{
    $("#slcDias").removeClass('is-invalid');
    guardarSeccion();

  }
});


function guardarSeccion(){
  var parametro='codigoAlterno='+$("#codigoAlterno").val()+'&horaInicio='+$("#horaInicio").val()+':00'+'&horaFinal='+$("#horaFinal").val()+':00'
                +'&asignatura='+$("#slcAsignatura").val()+'&aula='+$("#slcAula").val()+'&maestro='+$("#slcMaestro").val()+'&dias='+$("#slcDias").val()
                +'&periodo='+$("#slcPeriodo").val()+'&cupos='+$("#cupos").val();

                $.ajax({
                      url:"/guardarSeccion",
                      method:'POST',
                      data:parametro,
                      success:function(respuesta){
                            if(respuesta=='correcto'){
                              table_show
                              .clear()
                              listar();
                              swal("Acción realizada con éxito!", "", "success");
                            }else{
                              swal("Acción realizada sin éxito!", "", "error");
                            }
                      }
                      });

}
