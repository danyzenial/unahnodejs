


$(document).ready(function(){

    $.ajax({
      url:'/slcCarreras',
      success: function(answer){
        var contenido='<option value="0" selected disabled>Departamento</option>';
        for (var i = answer.length - 1; i >= 0; i--)
        {
            contenido+='<option value="'+answer[i].CODIGO_CARRERA+'">'+answer[i].NOMBRE_CARRERA+'</option>'
        }
        $("#slcCarrera").html(contenido);
        $("#slcCarrera2").html(contenido);
        $("#slcCarrera3").html(contenido);

      }
    });

    $.ajax({
      url:'/slcTipoAsignatura',
      success: function(answer){
        var contenido='<option value="0" selected disabled>Tipo</option>';
        for (var i = answer.length - 1; i >= 0; i--)
        {
            contenido+='<option value="'+answer[i].CODIGO_TIPO_ASIGNATURA+'">'+answer[i].TIPO_ASIGNATURA+'</option>'
        }
        $("#slcTipo").html(contenido);

      }
    });

});


$("#enviarAsignatura").click(function(){
  if($("#codigoAsignatura").val()==''){
    $("#codigoAsignatura").addClass('is-invalid');
  }else if ($("#nombreAsignatura").val()=='') {
    $("#codigoAsignatura").removeClass('is-invalid');
    $("#nombreAsignatura").addClass('is-invalid');
  }else if ($("#cantidadUvAsignatura").val()==''  || (/^[1-9][0-9]?$|^100$/.test($("#cantidadUvAsignatura").val()))!=true) {
    $("#nombreAsignatura").removeClass('is-invalid');
    $("#cantidadUvAsignatura").addClass('is-invalid');
  }else if ($("#slcCarrera").val()==0 || $("#slcCarrera").val()==null) {
    $("#cantidadUvAsignatura").removeClass('is-invalid');
    $("#slcCarrera").addClass('is-invalid');
  }else if ($("#slcTipo").val()==0 || $("#slcTipo").val()==null) {
    $("#slcCarrera").removeClass('is-invalid');
    $("#slcTipo").addClass('is-invalid');
  }else{
    guardarAsignatura();
    $("#cantidadUvAsignatura").removeClass('is-invalid');
    $("#codigoAsignatura").val('');
    $("#nombreAsignatura").val('');
    $("#cantidadUvAsignatura").val('');
    $("#slcTipo").val(0);
    $("#slcCarrera").val(0);
  }
});



$("#enviarAgregarCarrera").click(function(){
  var parametro='codigoAsignatura='+codigoAsignatura1+'&carrera='+$("#slcCarrera2").val();
    $.ajax({
      url: '/carreraAsignatura',
      method:'POST',
      data:parametro,
      success: function(answer) {
            table_show.clear();
            listar();
            swal("Acción realizada con éxito!", "", "success");
            $("#slcCarrera2").val(0);
          }
    })
});

$("#enviarRequisitoAsignatura").click(function(){
  var parametro='codigoAsignatura='+codigoAsignatura1+'&requisito='+$("#slcAsignatura").val()+'&carrera='+$("#slcCarrera3").val();
    $.ajax({
      url: '/requisitoAsignatura',
      method:'POST',
      data:parametro,
      success: function(answer) {
        table_show.clear();
        listar();
            swal("Acción realizada con éxito!", "", "success");
            $("#slcAsignatura").val(0);
            $("#slcCarrera3").val(0);
          }
    })
});




function guardarAsignatura(){

    var parametro='codigoAsignatura='+$("#codigoAsignatura").val()+'&nombreAsignatura='+$("#nombreAsignatura").val()
                  +'&cantidadUvAsignatura='+$("#cantidadUvAsignatura").val()+'&codigoCarrera='+$("#slcCarrera").val()+'&codigoTipo='+
                  $("#slcTipo").val();
      $.ajax({
        url: '/guardarAsignatura',
        method:'POST',
        data:parametro,
        success: function(answer) {
          table_show.clear();
          listar();
              swal("Acción realizada con éxito!", "", "success");
              $("#codigoAsignatura").val('');
              $("#nombreAsignatura").val('');
              $("#cantidadUvAsignatura").val('');
              $("#slcCarrera").val('');
              $("#slcTipo").val('');
            }
      })
}


var codigoAsignatura1;
var table_show =  $('#table1').DataTable({

   responsive: true,
  "language": {
  "sProcessing":     "Procesando...",
  "sLengthMenu":     "Mostrar _MENU_ registros",
  "sZeroRecords":    "No se encontraron resultados",
  "sEmptyTable":     "Ningún dato disponible en esta tabla",
  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
  "sInfoPostFix":    "",
  "sSearch":         "Buscar:",
  "sUrl":            "",
  "sInfoThousands":  ",",
  "sLoadingRecords": "Cargando...",
  "oPaginate": {
      "sFirst":    "Primero",
      "sLast":     "Último",
      "sNext":     "Siguiente",
      "sPrevious": "Anterior"
  },
  "oAria": {
      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  }
}
});

listar();

function listar(){

      $.ajax({
        url: '/cargarAsignaturas',
        success: function(answer) {
                  var contenido='<option value="0" selected disabled>Requisito</option>';
                  for (var i = answer.length - 1; i >= 0; i--)
                  {



                    contenido+='<option value="'+answer[i].CODIGO_ASIGNATURA+'">'+answer[i].NOMBRE_ASIGNATURA+'</option>'



                      var rowNode = table_show
                      .row.add([
                                  answer[i].CODIGO_ASIGNATURA,
                                  answer[i].CODIGO_ALTERNO,
                                  answer[i].NOMBRE_ASIGNATURA,
                                  answer[i].UV,
                                  answer[i].NOMBRE_CARRERA,
                                  '<button  data-toggle="modal" data-target="#modalRequisito" type="button" class="carrera btn btn-primary btn-sm"><i class="fas fa-plus-circle"></i> </button> ',
                                  '<button data-toggle="modal" data-target="#modalCarrera" type="button" class="carrera btn btn-success  btn-sm" id="eliminarRegistro1"><i class="fas fa-graduation-cap"></i></button>'
                              ])
                      .draw()
                      .node();
                  }
                  $("#slcAsignatura").html(contenido);

                }

      });

      obtenerData("#table1 tbody",table_show);

}

function obtenerData(tbody, table){
  $(tbody).on("click","button.carrera",function(){
    var data = table.row($(this).parents("tr")).data();
    codigoAsignatura1=data[0];
  });
}
