

$(document).ready(function(){
  $.ajax({
    url: '/slcPeriodo',
    success: function(answer) {

              var contenido='<option value="0" selected disabled>Período</option>';
              for (var i = answer.length - 1; i >= 0; i--)
              {
                  contenido+='<option value="'+answer[i].CODIGO_PERIODO+'">'+answer[i].NOMBRE_PERIODO+'</option>'
              }
              $("#slcPeriodo").html(contenido);

            }

  });
});


var codigoFecha;
var table_show =  $('#table1').DataTable({

  /*"ajax": {"url":"../PHP/responses.php",
  "dataSrc": ""},
  "columns":[
    {"data":"NOMBRE_EMPLEADO"},
    {"data":"Apellido_EMPLEADO"},
    {"data":"NOMBRE_ROL"},
    {"data":"NOMBRE_USUARIO"},
    {"data":"TELEFONO_EMPLEADO"},
    {"data":"CORREO_EMPLEADO"},
    {"defaultContent":'<button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#editarUsuarioModal" class="btn btn-success"> Editar</button><button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#asignarTopicoModal" class="btn btn-primary"> Asignar Tópico</button><button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#infoUsuarioModal" class="btn btn-warning"> Info</button>' }
  ],*/
   responsive: true,
  "language": {

  "sProcessing":     "Procesando...",
  "sLengthMenu":     "Mostrar _MENU_ registros",
  "sZeroRecords":    "No se encontraron resultados",
  "sEmptyTable":     "Ningún dato disponible en esta tabla",
  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
  "sInfoPostFix":    "",
  "sSearch":         "Buscar:",
  "sUrl":            "",
  "sInfoThousands":  ",",
  "sLoadingRecords": "Cargando...",
  "oPaginate": {
      "sFirst":    "Primero",
      "sLast":     "Último",
      "sNext":     "Siguiente",
      "sPrevious": "Anterior"
  },
  "oAria": {
      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  }
}
});


listar();

function listar(){

        $.ajax({
          url: '/cargarFechas',
          success: function(answer) {

                    var tipo;
                    for (var i = answer.length - 1; i >= 0; i--)
                    {

                        if (answer[i].ACCESO_MATRICULA==1) {
                            tipo='Fecha de Matrícula';
                        }else if (answer[i].ACCESO_MATRICULA==2) {
                            tipo='Fecha de Evaluación Docentes';
                        }else if (answer[i].ACCESO_MATRICULA==3) {
                            tipo='Fecha de Subir Calificaciones';
                        }else if (answer[i].ACCESO_MATRICULA==4) {
                            tipo='Fecha Ver Calificaciones';
                        }

                        var rowNode = table_show
                        .row.add([
                                    answer[i].ACCESO_MATRICULA,
                                    tipo,
                                    answer[i].NOMBRE_PERIODO,
                                    answer[i].FECHA_INICIO,
                                    answer[i].FECHA_FINAL,
                                    '<button  data-toggle="modal" data-target="#actualizarFecha" type="button" class="editar btn btn-primary btn-sm"><i class="fas fa-edit"></i> </button> '
                                ])
                        .draw()
                        .node();
                    }

                  }

        });

      obtenerData("#table1 tbody",table_show);

}

function obtenerData(tbody, table){
  $(tbody).on("click","button.editar",function(){
    var data = table.row($(this).parents("tr")).data();
    codigoFecha=data[0];
    $("#fecha1").val(data[3]);
    $("#fecha2").val(data[4]);
     $("#slcPeriodo option:contains("+data[2]+")").attr('selected', true);
    console.log(codigoFecha);
  });
}

$("#enviarFechaNueva").click(function(){
  var parametro='codigoFecha='+codigoFecha+'&fecha1='+$("#fecha1").val()+'&fecha2='+$("#fecha2").val()+'&periodo='+$("#slcPeriodo").val();
  $.ajax({
    url:'/actualizarAcceso',
    data:parametro,
    method: 'POST',
    success:function(respuesta){
        if(respuesta!=''){
          table_show.clear();
          listar();
            swal("Acción realizada con éxito!", "", "success")
        }else{
            swal("Acción realizada sin éxito!", "", "error")
        }
    }
  });

});
