


$( document ).ready(function() {

  console.log($("#slcFacultades").val());
  $.ajax({
    url: '/slcFacultades',
    success: function(answer) {

              var contenido='<option value="0" selected disabled>Facultad</option>';
              for (var i = answer.length - 1; i >= 0; i--)
              {
                  contenido+='<option value="'+answer[i].CODIGO_FACULTAD+'">'+answer[i].NOMBRE_FACULTAD+'</option>'
              }
              $("#slcFacultades").html(contenido);

            }

  });

  $.ajax({
    url: '/slcGrados',
    success: function(answer) {

              var contenido='<option value="0" selected disabled>Grado</option>';
              for (var i = answer.length - 1; i >= 0; i--)
              {
                  contenido+='<option value="'+answer[i].CODIGO_GRADO+'">'+answer[i].NOMBRE_GRADO+'</option>'
              }
              $("#slcGrados").html(contenido);

            }

  });

  var codCarrera;
  var table_show =  $('#table1').DataTable({

    /*"ajax": {"url":"../PHP/responses.php",
    "dataSrc": ""},
    "columns":[
      {"data":"NOMBRE_EMPLEADO"},
      {"data":"Apellido_EMPLEADO"},
      {"data":"NOMBRE_ROL"},
      {"data":"NOMBRE_USUARIO"},
      {"data":"TELEFONO_EMPLEADO"},
      {"data":"CORREO_EMPLEADO"},
      {"defaultContent":'<button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#editarUsuarioModal" class="btn btn-success"> Editar</button><button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#asignarTopicoModal" class="btn btn-primary"> Asignar Tópico</button><button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#infoUsuarioModal" class="btn btn-warning"> Info</button>' }
    ],*/
     responsive: true,
    "language": {

    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
  }
});

listar();

function listar(){

        $.ajax({
          url: '/cargarCarreras',
          success: function(answer) {

                    for (var i = answer.length - 1; i >= 0; i--)
                    {
                        var rowNode = table_show
                        .row.add([
                                    answer[i].CODIGO_CARRERA,
                                    answer[i].CODIGO_AUXILIAR,
                                    answer[i].NOMBRE_FACULTAD,
                                    answer[i].NOMBRE_CARRERA,
                                    answer[i].CANTIDAD_ASIGNATURAS,
                                    answer[i].CANTIDAD_UNIDADES_VALORATIVAS,
                                    answer[i].NOMBRE_GRADO,
                                    '<button  data-toggle="modal" data-target="#adicionarCarrera" type="button" class="editar btn btn-primary btn-sm"><i class="fas fa-edit"></i> </button> '+
                                    '<button data-toggle="modal" data-target="#confirmarRespuesta" type="button" class="eliminar btn btn-danger  btn-sm" id="eliminarRegistro1"><i class="fas fa-trash-alt"></i></button>'
                                ])
                        .draw()
                        .node();
                    }

                  }

        });

      obtenerData("#table1 tbody",table_show);
      obtenerDataDelete("#table1 tbody",table_show);

}

function obtenerData(tbody, table){
  $(tbody).on("click","button.editar",function(){
    $("#actualizar").removeAttr('hidden');
    $("#enviarCarrera").attr('hidden',true);
    var data = table.row($(this).parents("tr")).data();
    console.log(data);
    $("#codigoAuxiliar").val(data[1]);
    $("#nombreCarrera").val(data[3]);
    $("#cantidadAsignaturas").val(data[4]);
    $("#cantidadUV").val(data[5]);

    codCarrera=data[0];
    console.log($("#slcFacultades").val());
  });
}

function obtenerDataDelete(tbody, table){
  $(tbody).on("click","button.eliminar",function(){
    var data = table.row($(this).parents("tr")).data();
    console.log(data[0]);
    codCarrera=data[0];
  });
}


function guardarCarrera(){
  var parametros = 'codigoAuxiliar='+$("#codigoAuxiliar").val()+"&codigoFacultad="+$("#slcFacultades").val()+
                  '&nombreCarrera='+$("#nombreCarrera").val()+'&cantidadAsignaturas='+$("#cantidadAsignaturas").val()+
                  '&cantidadUV='+$("#cantidadUV").val()+'&codigoGrado='+$("#slcGrados").val();
  $.ajax({
        url:"/guardarCarrera",
        method:'POST',
        data:parametros,
        success:function(respuesta){

            table_show.clear();
            listar();
              console.log(respuesta);

        }
        });
}

$("#actualizar").click(function(){

  var parametros = 'codigoCarrera='+codCarrera+'&codigoAuxiliar='+$("#codigoAuxiliar").val()+"&codigoFacultad="+$("#slcFacultades").val()+
                  '&nombreCarrera='+$("#nombreCarrera").val()+'&cantidadAsignaturas='+$("#cantidadAsignaturas").val()+
                  '&cantidadUV='+$("#cantidadUV").val()+'&codigoGrado='+$("#slcGrados").val();

  $.ajax({
    url: '/actualizarCarrera',
    method:'POST',
    data:parametros,
    success: function(answer) {
      table_show.clear();
      listar();

        }
  })





});

$("#cerrarModal").click(function(){
  $("#enviarCarrera").removeAttr('hidden');
  $("#actualizar").attr('hidden',true);
});

$("#modal-btn-si").on("click", function(){
  $("#confirmarRespuesta").modal('hide');
  var parametros='codCarrera='+codCarrera;
  $.ajax({
    url: '/eliminarCarrera',
    method:'POST',
    data:parametros,
    success: function(answer) {
          table_show.clear();
          listar();
        }

  });

});

$("#modal-btn-no").on("click", function(){
  $("#confirmarRespuesta").modal('hide');
});


$("#enviarCarrera").click(function() {


  if($("#codigoAuxiliar").val()==''){
      $("#codigoAuxiliar").addClass('is-invalid');
  }else if ($("#nombreCarrera").val()=='') {
    $("#codigoAuxiliar").removeClass('is-invalid');
    $('#nombreCarrera').addClass('is-invalid');
  }else if ($("#cantidadAsignaturas").val()=='' || (/^[1-9][0-9]?$|^100$/.test($("#cantidadAsignaturas").val()))!=true) {
    $('#nombreCarrera').removeClass('is-invalid');
    $("#cantidadAsignaturas").addClass('is-invalid');
  }else if ($("#cantidadUV").val()=='' || (/^[1-9][0-9][0-9]?$|^100$/.test($("#cantidadUV").val()))!=true) {
    $("#cantidadAsignaturas").removeClass('is-invalid');
    $("#cantidadUV").addClass('is-invalid');
  }else if ($("#slcFacultades").val()==null) {
    $("#cantidadUV").removeClass('is-invalid');
    $("#slcFacultades").addClass('is-invalid');
  }else if ($("#slcGrados").val()==null) {
    $("#slcFacultades").removeClass('is-invalid');
    $("#slcGrados").addClass('is-invalid');
  }else{
    $("#slcGrados").removeClass('is-invalid');
    guardarCarrera();
    table_show.clear();
    listar();
    $("#codigoAuxiliar").val('');
    $("#nombreCarrera").val('');
    $("#cantidadAsignaturas").val('');
    $("#cantidadUV").val('');
  }

});

});



$("#slcFacultades").change(function(){
  console.log($("#slcFacultades").val());
});

$("#slcGrados").change(function(){
  console.log($("#slcGrados").val());

});
