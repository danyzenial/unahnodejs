

$(document).ready(function(){

  $.ajax({
    url: '/slcCampus',
    success: function(answer) {

              var contenido='<option value="0" selected disabled>Campus</option>';
              for (var i = answer.length - 1; i >= 0; i--)
              {
                  contenido+='<option value="'+answer[i].CODIGO_CAMPUS+'">'+answer[i].NOMBRE_CAMPUS+'</option>'
              }
              $("#slcCampus").html(contenido);

            }

  });

  $("#slcCampus").change(function(){
    var parametros = 'codigoCampus='+$("#slcCampus").val();
    $.ajax({
      url: '/slcEdificios',
      method:'POST',
      data:parametros,
      success: function(answer) {

                var contenido='<option value="0" selected disabled>Edificio</option>';
                for (var i = answer.length - 1; i >= 0; i--)
                {
                    contenido+='<option value="'+answer[i].CODIGO_EDIFICIO+'">'+answer[i].NOMBRE_EDIFICIO+'</option>'
                }
                $("#slcEdificio").html(contenido);

              }

    });
  });

  $.ajax({
    url: '/slcAulas',
    success: function(answer) {

              var contenido='<option value="0" selected disabled>Tipo Aula</option>';
              for (var i = answer.length - 1; i >= 0; i--)
              {
                  contenido+='<option value="'+answer[i].CODIGO_TIPO_AULA+'">'+answer[i].TIPO_AULA+'</option>'
              }
              $("#slcAula").html(contenido);

            }

  });


  var codigoAula;
  var table_show =  $('#table1').DataTable({

    /*"ajax": {"url":"../PHP/responses.php",
    "dataSrc": ""},
    "columns":[
      {"data":"NOMBRE_EMPLEADO"},
      {"data":"Apellido_EMPLEADO"},
      {"data":"NOMBRE_ROL"},
      {"data":"NOMBRE_USUARIO"},
      {"data":"TELEFONO_EMPLEADO"},
      {"data":"CORREO_EMPLEADO"},
      {"defaultContent":'<button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#editarUsuarioModal" class="btn btn-success"> Editar</button><button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#asignarTopicoModal" class="btn btn-primary"> Asignar Tópico</button><button type="button" name="button" id="ingresarUsuario" data-toggle="modal" data-target="#infoUsuarioModal" class="btn btn-warning"> Info</button>' }
    ],*/
     responsive: true,
    "language": {

    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
  }
});

listar();

function listar(){

        $.ajax({
          url: '/cargarAulas',
          success: function(answer) {

                    for (var i = answer.length - 1; i >= 0; i--)
                    {
                        var rowNode = table_show
                        .row.add([
                                    answer[i].CODIGO_AULA,
                                    answer[i].NUMERO_AULA,
                                    answer[i].TIPO_AULA,
                                    answer[i].NOMBRE_EDIFICIO,
                                    answer[i].NOMBRE_CAMPUS,
                                    '<button  data-toggle="modal" data-target="#adicionarAula" type="button" class="editar btn btn-primary btn-sm"><i class="fas fa-edit"></i> </button> '+
                                    '<button data-toggle="modal" data-target="#confirmarRespuesta" type="button" class="eliminar btn btn-danger  btn-sm" id="eliminarRegistro1"><i class="fas fa-trash-alt"></i></button>'
                                ])
                        .draw()
                        .node();
                    }

                  }

        });

      obtenerData("#table1 tbody",table_show);
      obtenerDataDelete("#table1 tbody",table_show);

}

function obtenerData(tbody, table){
  $(tbody).on("click","button.editar",function(){
    $("#actualizarAula").removeAttr('hidden');
    $("#enviarAula").attr('hidden',true);
    var data = table.row($(this).parents("tr")).data();
    console.log(data);
    $("#numeroAula").val(data[1]);

    codigoAula=data[0];

  });
}

function obtenerDataDelete(tbody, table){
  $(tbody).on("click","button.eliminar",function(){
    var data = table.row($(this).parents("tr")).data();
    console.log(data[0]);
    codigoAula=data[0];
  });
}


function guardarAula(){
  var parametros = 'numeroAula='+$("#numeroAula").val()+"&codigoTipoAula="+$("#slcAula").val()+
                    "&codigoEdificio="+$("#slcEdificio").val();
  $.ajax({
        url:"/guardarAula",
        method:'POST',
        data:parametros,
        success:function(respuesta){
          table_show.clear();
          listar();
              swal("Acción realizada con éxito!", "", "success");
              console.log(respuesta);

        }
        });
}

$("#actualizarAula").click(function(){

  var parametros = 'numeroAula='+$("#numeroAula").val()+"&codigoTipoAula="+$("#slcAula").val()+
                    "&codigoEdificio="+$("#slcEdificio").val()+'&codigoAula='+codigoAula;

  $.ajax({
    url: '/actualizarAula',
    method:'POST',
    data:parametros,
    success: function(answer) {
      table_show.clear();
      listar();
      swal("Acción realizada con éxito!", "", "success");
        }
  })

});

$("#cerrarModal").click(function(){
  $("#enviarAula").removeAttr('hidden');
  $("#actualizarAula").attr('hidden',true);
});

$("#modal-btn-si").on("click", function(){
  $("#confirmarRespuesta").modal('hide');
  var parametros='codigoAula='+codigoAula;
  $.ajax({
    url: '/eliminarAula',
    method:'POST',
    data:parametros,
    success: function(answer) {
          table_show.clear();
          listar();
          swal("Acción realizada con éxito!", "", "success");
        }

  });

});

$("#modal-btn-no").on("click", function(){
  $("#confirmarRespuesta").modal('hide');
});



  $("#enviarAula").click(function(){
    if($("#numeroAula").val()==''){
      $("#numeroAula").addClass('is-invalid');
    }else if ($("#slcAula").val()==null) {
        $("#numeroAula").removeClass('is-invalid');
        $("#slcAula").addClass('is-invalid');
    }else if ($("#slcCampus").val()==null) {
        $("#slcAula").removeClass('is-invalid');
        $("#slcCampus").addClass('is-invalid');
    }else if ($("#slcEdificio").val()==null) {
        $("#slcEdificio").addClass('is-invalid');
        $("#slcCampus").removeClass('is-invalid');
    }else{
      $("#slcEdificio").removeClass('is-invalid');
      guardarAula();
      table_show.clear();
      listar();
      $("#numeroAula").val('');
    }
  });

});





/*$("#enviarTipoAula").click(function(){
  if($("#nombreTipoAula").val()==''){
    $("#nombreTipoAula").addClass('is-invalid');
  }else if ($("#descripcionTipoAula").val()=='') {
    $("#descripcionTipoAula").addClass('is-invalid');
    $("#nombreTipoAula").removeClass('is-invalid');
  }else{
    $("#descripcionTipoAula").removeClass('is-invalid');
    $("#descripcionTipoAula").val('');
    $("#nombreTipoAula").val('');
    alert('Tipo de Aula Insertada');
  }
});*/
