const express = require('express');
var session = require("express-session");
const mysql = require('mysql');
const bodyParser = require('body-parser');


var app = express();

app.use(express.static('public'));
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({secret:"ASDFE$%#%",resave:true, saveUninitialized:true}));


var credencialesBD = {  host    : '127.0.0.1',
                      user    : 'root',
                      port    : '3306',
                      password: '',
                      database: 'unah' };



app.get('/',function(peticion,respuesta){

});




///////////-----------------------------------FACULTADES----------------------------------------//////////////////




app.post('/guardarFacultad',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query(
  "INSERT INTO  tbl_facultades ( CODIGO_FACULTAD ,  NOMBRE_FACULTAD ,  DESCRIPCION ) VALUES (NULL,?,?)",
  [
    peticion.body.nombreFacultad,
    peticion.body.descripcionFacultad
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      respuesta.send('correcto');
      conexion.end();
    }

  });


});

app.get('/cargarFacultades',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("SELECT CODIGO_FACULTAD, NOMBRE_FACULTAD, DESCRIPCION FROM tbl_facultades",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/actualizarFacultades',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("UPDATE   tbl_facultades   SET   NOMBRE_FACULTAD =? ,  DESCRIPCION  = ?  WHERE CODIGO_FACULTAD= ?",[
            peticion.body.nombreFacultad1,
            peticion.body.descripcionFacultad1,
            peticion.body.idFacultad
            ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/eliminarFacultad',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("DELETE FROM tbl_facultades WHERE CODIGO_FACULTAD=?",[
            peticion.body.idFacultad
            ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

////////////-----------------------------------CARRERAS-----------------------------------------//////////////////
app.get('/slcFacultades',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select CODIGO_FACULTAD, NOMBRE_FACULTAD from tbl_facultades",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.get('/slcGrados',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select CODIGO_GRADO, NOMBRE_GRADO from tbl_grados",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.get('/cargarCarreras',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select a.CODIGO_CARRERA, a.CODIGO_AUXILIAR,  b.NOMBRE_FACULTAD, a.NOMBRE_CARRERA,a.CANTIDAD_ASIGNATURAS, a.CANTIDAD_UNIDADES_VALORATIVAS,"+
                        "c.NOMBRE_GRADO "+
                        "from tbl_carreras a "+
                        "inner join tbl_facultades b on a.CODIGO_FACULTAD=b.CODIGO_FACULTAD "+
                        "inner  join tbl_grados c on a.CODIGO_GRADO=c.CODIGO_GRADO ",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/guardarCarrera',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query(
                            "INSERT INTO   tbl_carreras  (  CODIGO_CARRERA  ,   CODIGO_AUXILIAR  ,   CODIGO_FACULTAD  ,   NOMBRE_CARRERA  ,  "+
                                                            "CANTIDAD_ASIGNATURAS  ,  CANTIDAD_UNIDADES_VALORATIVAS  ,   CODIGO_GRADO  ) "+
                            " VALUES (NULL,?,?,?,?,?,?)",
  [
    peticion.body.codigoAuxiliar,
    peticion.body.codigoFacultad,
    peticion.body.nombreCarrera,
    peticion.body.cantidadAsignaturas,
    peticion.body.cantidadUV,
    peticion.body.codigoGrado
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      respuesta.send('correcto');
      conexion.end();
    }

  });


});

app.post('/actualizarCarrera',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("UPDATE  tbl_carreras  SET   CODIGO_AUXILIAR =?, CODIGO_FACULTAD =?, "+
                            "NOMBRE_CARRERA =?, CANTIDAD_ASIGNATURAS =?, CANTIDAD_UNIDADES_VALORATIVAS =?, "+
                            "CODIGO_GRADO =? WHERE CODIGO_CARRERA=?",
            [
              peticion.body.codigoAuxiliar,
              peticion.body.codigoFacultad,
              peticion.body.nombreCarrera,
              peticion.body.cantidadAsignaturas,
              peticion.body.cantidadUV,
              peticion.body.codigoGrado,
              peticion.body.codigoCarrera
            ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/eliminarCarrera',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("DELETE FROM tbl_carreras WHERE CODIGO_CARRERA=?",[
            peticion.body.codCarrera
            ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

////////////-----------------------------------CAMPUS-----------------------------------------//////////////////
app.get('/cargarCampus',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("SELECT   CODIGO_CAMPUS  ,   NOMBRE_CAMPUS   FROM   tbl_campus ",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/guardarCampus',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("INSERT INTO    tbl_campus   (   CODIGO_CAMPUS   ,    NOMBRE_CAMPUS   ) VALUES (NULL,?)",
  [
    peticion.body.nombreCampus
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      respuesta.send('correcto');
      conexion.end();
    }

  });


});

app.post('/actualizarCampus',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("UPDATE    tbl_campus    SET   NOMBRE_CAMPUS   = ? WHERE CODIGO_CAMPUS= ?",
            [
              peticion.body.nombreCampus,
              peticion.body.codigoCampus
            ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/eliminarCampus',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("DELETE FROM tbl_campus WHERE CODIGO_CAMPUS=?",[
            peticion.body.codigoCampus
            ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

////////////-----------------------------------EDIFICIOS-----------------------------------------//////////////////
app.get('/slcCampus',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select CODIGO_CAMPUS, NOMBRE_CAMPUS from tbl_campus",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});


app.get('/cargarEdificios',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select a.CODIGO_EDIFICIO, a.NOMBRE_EDIFICIO, a.ALIAS_EDIFICIO, b.NOMBRE_CAMPUS "+
                  "from tbl_edificios a "+
                  "inner join tbl_campus b on a.CODIGO_CAMPUS = b.CODIGO_CAMPUS",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/guardarEdificio',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("INSERT INTO    tbl_edificios   (   CODIGO_EDIFICIO   ,    NOMBRE_EDIFICIO   ,    ALIAS_EDIFICIO   ,    CODIGO_CAMPUS   ) VALUES (NULL,?,?,?)",
  [
    peticion.body.nombreEdificio,
    peticion.body.aliasEdificio,
    peticion.body.codigoCampus
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      respuesta.send('correcto');
      conexion.end();
    }

  });
});

app.post('/actualizarEdificio',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("UPDATE    tbl_edificios    SET       NOMBRE_EDIFICIO   =?,   ALIAS_EDIFICIO   =?,   CODIGO_CAMPUS   =? WHERE CODIGO_EDIFICIO=?",
            [
              peticion.body.nombreEdificio,
              peticion.body.aliasEdificio,
              peticion.body.codigoCampus,
              peticion.body.codigoEdificio
            ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/eliminarEdificio',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("DELETE FROM tbl_edificios WHERE CODIGO_EDIFICIO=?",[
            peticion.body.codigoEdificio
            ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

////////////-----------------------------------AULAS-----------------------------------------//////////////////
app.get('/slcAulas',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select CODIGO_TIPO_AULA, TIPO_AULA from tbl_tipos_aulas",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/slcEdificios',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select CODIGO_EDIFICIO, NOMBRE_EDIFICIO from tbl_edificios where CODIGO_CAMPUS = ?",
  [
    peticion.body.codigoCampus
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.get('/cargarAulas',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select a.CODIGO_AULA, a.NUMERO_AULA, b.TIPO_AULA, c.NOMBRE_EDIFICIO, d.NOMBRE_CAMPUS "
                +"from tbl_aulas a "
                +"inner join tbl_tipos_aulas b on a.CODIGO_TIPO_AULA = b.CODIGO_TIPO_AULA "
                +"inner join tbl_edificios c on a.CODIGO_EDIFICIO = c.CODIGO_EDIFICIO "
                +"inner join tbl_campus d on c.CODIGO_CAMPUS = d.CODIGO_CAMPUS",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/guardarAula',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("INSERT INTO    tbl_aulas   (   CODIGO_AULA   ,    NUMERO_AULA   ,    CODIGO_TIPO_AULA   ,    CODIGO_EDIFICIO   ) VALUES (NULL,?,?,?)",
  [
    peticion.body.numeroAula,
    peticion.body.codigoTipoAula,
    peticion.body.codigoEdificio
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      respuesta.send('correcto');
      conexion.end();
    }

  });

});

app.post('/actualizarAula',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("UPDATE    tbl_aulas    SET    NUMERO_AULA   =?,   CODIGO_TIPO_AULA   =?,   CODIGO_EDIFICIO   =? WHERE CODIGO_AULA = ?",
            [
              peticion.body.numeroAula,
              peticion.body.codigoTipoAula,
              peticion.body.codigoEdificio,
              peticion.body.codigoAula
            ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});


app.post('/eliminarAula',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("DELETE FROM tbl_aulas WHERE CODIGO_AULA=?",[
            peticion.body.codigoAula
            ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

////////////-----------------------------------PERSONAS----------------------------------------//////////////////

app.get('/slcGenero',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("SELECT CODIGO_GENERO, NOMBRE_GENERO FROM tbl_generos",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.get('/slcCivil',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("SELECT CODIGO_ESTADO_CIVIL, NOMBRE_ESTADO_CIVIL FROM tbl_estados_civiles",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.get('/slcIdentificacion',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("SELECT CODIGO_TIPO_IDENTIFICACION, TIPO_IDENTIFICACION FROM tbl_tipo_identificacion",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});
/*select CODIGO_LUGAR, NOMBRE_LUGAR from tbl_lugares where CODIGO_TIPO_LUGAR=1*/
app.get('/slcLugar',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select CODIGO_LUGAR, NOMBRE_LUGAR from tbl_lugares where CODIGO_TIPO_LUGAR=1",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/slcMunicipio',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select CODIGO_LUGAR, NOMBRE_LUGAR from tbl_lugares where CODIGO_TIPO_LUGAR=2 and CODIGO_LUGAR_PADRE=?",[
    peticion.body.codigoLugar
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.get('/slcCarreras',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select CODIGO_CARRERA, NOMBRE_CARRERA from tbl_carreras",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/verificarCuenta',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select * from tbl_alumnos where NUMERO_CUENTA=?",
  [
    peticion.body.nCuenta
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});

app.post('/cambioContrasenia',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("UPDATE tbl_alumnos SET PASSWORD=SHA1(?) WHERE CODIGO_ALUMNO=?",
  [
    peticion.body.newPassword,
    peticion.body.codAlumno
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});

app.post('/actualizarDatosPersona',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("UPDATE tbl_personas SET CODIGO_LUGAR_RESIDENCIA=?,CODIGO_MUNICIPIO_RESIDENCIA=?,DIRECCION=?,TELEFONO=?,CORREO_ELECTRONICO=? WHERE CODIGO_PERSONA=?",
  [
    peticion.body.resPersona,
    peticion.body.munPersona,
    peticion.body.direccionPersona,
    peticion.body.telefonoPersona,
    peticion.body.correoPersona,
    peticion.body.codigoPersona
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});

app.post('/guardarEstudiante',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query(
  "INSERT INTO tbl_personas(CODIGO_PERSONA, CODIGO_GENERO, CODIGO_LUGAR_NACIMIENTO,"+
                            "CODIGO_LUGAR_RESIDENCIA, CODIGO_MUNICIPIO_NACIMIENTO,"+
                            "CODIGO_MUNICIPIO_RESIDENCIA, CODIGO_CAMPUS, CODIGO_TIPO_IDENTIFICACION,"+
                            "CODIGO_ESTADO_CIVIL, NOMBRE, APELLIDO, FECHA_NACIMIENTO, IDENTIDAD,"+
                            "DIRECCION, TELEFONO, CORREO_ELECTRONICO) "+
                            " VALUES "+
                            "(NULL,?,?,?,?,?,?,?,?,?,?,SYSDATE(),?,?,?,?)",
  [
    peticion.body.codigoGenero,
    peticion.body.lugarNacimiento,
    peticion.body.lugarResidencia,
    peticion.body.municioNacimiento,
    peticion.body.municioResidencia,
    peticion.body.campus,
    peticion.body.identificacion,
    peticion.body.estadoCivil,
    peticion.body.nombre,
    peticion.body.apellido,
    peticion.body.identidad,
    peticion.body.direccion,
    peticion.body.telefono,
    peticion.body.email
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      id=resultado.insertId;
      respuesta.send('correcto');
      var conexion2 = mysql.createConnection(credencialesBD);
      conexion2.query("INSERT INTO tbl_carreras_x_alumnos"
                                  +"(CODIGO_CARRERA, CODIGO_ALUMNO, FECHA_REGISTRO_CARRERA,"
                                  +"PROMEDIO_CARRERA, CANTIDAD_CLASES_APROBADAS)"
                                  +"VALUES (?,?,sysdate(),0,0)",
                                  [
                                    peticion.body.carrera,
                                    id
                                  ],
              function(error, informacion, campos){
                if (error) throw error;

              }
        );

        var conexion3 = mysql.createConnection(credencialesBD);
        conexion3.query("INSERT INTO tbl_alumnos(CODIGO_ALUMNO, NUMERO_CUENTA, PUNTAJE_PAA, PROMEDIO, PASSWORD) "+
                                        "VALUES (?,?,?,0,SHA1(?))",
                                    [
                                      id,
                                      peticion.body.numeroCuenta,
                                      peticion.body.paa,
                                      peticion.body.contrasenia
                                    ],
                function(error, informacion, campos){
                  if (error) throw error;

                }
          );
    }

  });


});


app.get('/cargarEstudiantes',function(peticion, respuesta){

  var conexion1 = mysql.createConnection(credencialesBD);
    var conexion2 = mysql.createConnection(credencialesBD);
    var memes = [];
    var sql = "select a.CODIGO_ALUMNO, a.NUMERO_CUENTA, a.PROMEDIO,"+
                      "b.NOMBRE, b.APELLIDO, c.NOMBRE_GENERO,d.NOMBRE_LUGAR as LUGAR_NACIMIENTO,"+
                      "e.NOMBRE_LUGAR as LUGAR_RESIDENCIA, f.NOMBRE_LUGAR as MUNICIPIO_NACIMIENTO,"+
                      "g.NOMBRE_LUGAR as MUNICIPIO_RESIDENCIA, h.NOMBRE_CAMPUS, i.TIPO_IDENTIFICACION, j.NOMBRE_ESTADO_CIVIL,"+
                      "b.CORREO_ELECTRONICO, b.DIRECCION, b.FECHA_NACIMIENTO, b.IDENTIDAD, b.TELEFONO "+
              "from tbl_alumnos a "+
              "inner join tbl_personas b on a.CODIGO_ALUMNO = b.CODIGO_PERSONA "+
              "inner join  tbl_generos c on b.CODIGO_GENERO = c.CODIGO_GENERO "+
              "inner join tbl_lugares d on b.CODIGO_LUGAR_NACIMIENTO = d.CODIGO_LUGAR "+
              "inner join tbl_lugares e on b.CODIGO_LUGAR_RESIDENCIA = e.CODIGO_LUGAR "+
              "inner join tbl_lugares f on b.CODIGO_MUNICIPIO_NACIMIENTO = f.CODIGO_LUGAR "+
              "inner join tbl_lugares g on b.CODIGO_MUNICIPIO_RESIDENCIA = g.CODIGO_LUGAR "+
              "inner join tbl_campus h on b.CODIGO_CAMPUS = h.CODIGO_CAMPUS "+
              "inner join tbl_tipo_identificacion i on b.CODIGO_TIPO_IDENTIFICACION = i.CODIGO_TIPO_IDENTIFICACION "+
              "inner join tbl_estados_civiles j on b.CODIGO_ESTADO_CIVIL = j.CODIGO_ESTADO_CIVIL ";
    conexion1.query(sql)
    .on('result', function(meme){
        meme.comentarios=[];
        memes.push(meme);
        conexion1.pause();
        conexion2.query("select  b.NOMBRE_CARRERA "+
                          "from tbl_carreras_x_alumnos a "+
                          "inner join tbl_carreras b on a.CODIGO_CARRERA = b.CODIGO_CARRERA "+
                          "where a.CODIGO_ALUMNO=?",
                          [meme.CODIGO_ALUMNO])
        .on('result', function(comentario){
            meme.comentarios.push(comentario);
        })
        .on('end',function(){
            conexion1.resume();
        });
    })
    .on('end',function(){
        conexion1.end();
        conexion2.end();
        respuesta.send(memes);
});


});

////////////-------------------------------PERSONAL-----------------------------------////////////////////////

app.post('/verificarCuentaEmpleado',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select * from tbl_empleados where NUMERO_EMPLEADO=?",
  [
    peticion.body.nEmpleado
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});


app.post('/cambioContrasenia1',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("UPDATE tbl_empleados SET PASSWORD=SHA1(?) WHERE CODIGO_EMPLEADO=?",
  [
    peticion.body.newPassword,
    peticion.body.codPersona
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});


app.get('/slcTipoEmpleado',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select CODIGO_TIPO_EMPLEADO, NOMBRE_TIPO_EMPLEADO from tbl_tipo_empleado",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});



app.get('/slcTitulo',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select CODIGO_TITULO, NOMBRE_TITULO from tbl_titulos",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.get('/slcUniversidad',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select CODIGO_UNIVERSIDAD, NOMBRE_UNIVERSIDAD from tbl_universidades a",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.get('/slcCargo',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select CODIGO_CARGO, NOMBRE_CARGO from tbl_cargos",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/actualizarDatosPersona1',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("UPDATE tbl_personas SET CODIGO_LUGAR_RESIDENCIA=?,CODIGO_MUNICIPIO_RESIDENCIA=?,DIRECCION=?,TELEFONO=?,CORREO_ELECTRONICO=? WHERE CODIGO_PERSONA=?",
  [
    peticion.body.resPersona,
    peticion.body.munPersona,
    peticion.body.direccionPersona,
    peticion.body.telefonoPersona,
    peticion.body.correoPersona,
    peticion.body.codigoPersona
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

    var conexion2 = mysql.createConnection(credencialesBD);
    conexion2.query("UPDATE tbl_empleados SET CODIGO_TIPO_EMPLEADO=? WHERE CODIGO_EMPLEADO=?",
    [
      peticion.body.tipoEmpleado,
      peticion.body.codigoPersona
    ],
            function(error, informacion, campos){
              if (error) throw error;
              //respuesta.send(informacion);
              conexion2.end();
            }
      );


});

app.get('/cargarPersonal',function(peticion, respuesta){

  var conexion1 = mysql.createConnection(credencialesBD);
    var conexion2 = mysql.createConnection(credencialesBD);
    var memes = [];
    var sql = "select a.CODIGO_EMPLEADO, a.NUMERO_EMPLEADO, "+
                      "b.NOMBRE, b.APELLIDO, c.NOMBRE_GENERO,l.NOMBRE_CARGO,a.SUELDO_BASE,d.NOMBRE_LUGAR as LUGAR_NACIMIENTO, "+
                      "e.NOMBRE_LUGAR as LUGAR_RESIDENCIA, f.NOMBRE_LUGAR as MUNICIPIO_NACIMIENTO, "+
                      "g.NOMBRE_LUGAR as MUNICIPIO_RESIDENCIA, h.NOMBRE_CAMPUS, i.TIPO_IDENTIFICACION, j.NOMBRE_ESTADO_CIVIL, "+
                      "b.CORREO_ELECTRONICO, b.DIRECCION, b.FECHA_NACIMIENTO, b.IDENTIDAD, b.TELEFONO, k.NOMBRE_TIPO_EMPLEADO "+
              "from tbl_empleados a "+
              "inner join tbl_personas b on a.CODIGO_EMPLEADO = b.CODIGO_PERSONA "+
              "inner join  tbl_generos c on b.CODIGO_GENERO = c.CODIGO_GENERO "+
              "inner join tbl_lugares d on b.CODIGO_LUGAR_NACIMIENTO = d.CODIGO_LUGAR "+
              "inner join tbl_lugares e on b.CODIGO_LUGAR_RESIDENCIA = e.CODIGO_LUGAR "+
              "inner join tbl_lugares f on b.CODIGO_MUNICIPIO_NACIMIENTO = f.CODIGO_LUGAR "+
              "inner join tbl_lugares g on b.CODIGO_MUNICIPIO_RESIDENCIA = g.CODIGO_LUGAR "+
              "inner join tbl_campus h on b.CODIGO_CAMPUS = h.CODIGO_CAMPUS "+
              "inner join tbl_tipo_identificacion i on b.CODIGO_TIPO_IDENTIFICACION = i.CODIGO_TIPO_IDENTIFICACION "+
              "inner join tbl_estados_civiles j on b.CODIGO_ESTADO_CIVIL = j.CODIGO_ESTADO_CIVIL "+
              "inner join tbl_cargos l on a.CODIGO_CARGO = l.CODIGO_CARGO "+
              "inner join tbl_tipo_empleado k on a.CODIGO_TIPO_EMPLEADO = k.CODIGO_TIPO_EMPLEADO";
    conexion1.query(sql)
    .on('result', function(meme){
        meme.comentarios=[];
        memes.push(meme);
        conexion1.pause();
        conexion2.query("select b.NOMBRE_TITULO "+
                          "from tbl_titulos_x_empleado a "+
                          "inner join tbl_titulos b on a.CODIGO_TITULO = b.CODIGO_TITULO "+
                          "where CODIGO_EMPLEADO=?",
                          [meme.CODIGO_EMPLEADO])
        .on('result', function(comentario){
            meme.comentarios.push(comentario);
        })
        .on('end',function(){
            conexion1.resume();
        });
    })
    .on('end',function(){
        conexion1.end();
        conexion2.end();
        respuesta.send(memes);
});


});


app.post('/guardarPersonal',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query(
  "INSERT INTO tbl_personas(CODIGO_PERSONA, CODIGO_GENERO, CODIGO_LUGAR_NACIMIENTO,"+
                            "CODIGO_LUGAR_RESIDENCIA, CODIGO_MUNICIPIO_NACIMIENTO,"+
                            "CODIGO_MUNICIPIO_RESIDENCIA, CODIGO_CAMPUS, CODIGO_TIPO_IDENTIFICACION,"+
                            "CODIGO_ESTADO_CIVIL, NOMBRE, APELLIDO, FECHA_NACIMIENTO, IDENTIDAD,"+
                            "DIRECCION, TELEFONO, CORREO_ELECTRONICO) "+
                            " VALUES "+
                            "(NULL,?,?,?,?,?,?,?,?,?,?,SYSDATE(),?,?,?,?)",
  [
    peticion.body.codigoGenero,
    peticion.body.lugarNacimiento,
    peticion.body.lugarResidencia,
    peticion.body.municioNacimiento,
    peticion.body.municioResidencia,
    peticion.body.campus,
    peticion.body.identificacion,
    peticion.body.estadoCivil,
    peticion.body.nombre,
    peticion.body.apellido,
    peticion.body.identidad,
    peticion.body.direccion,
    peticion.body.telefono,
    peticion.body.email
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      id=resultado.insertId;

      respuesta.send('correcto');

      var conexion2 = mysql.createConnection(credencialesBD);
      conexion2.query("INSERT INTO tbl_empleados(CODIGO_EMPLEADO, NUMERO_EMPLEADO, "
                                    +"PASSWORD, SUELDO_BASE, CODIGO_TIPO_EMPLEADO, CODIGO_CARGO) "
                                    +"VALUES (?,?,SHA1(?),?,1,?)",
                                  [
                                    id,
                                    peticion.body.numeroEmpleado,
                                    peticion.body.contrasenia,
                                    peticion.body.sueldo,
                                    peticion.body.cargoPesonal
                                  ],
              function(error, informacion, campos){
                if (error) throw error;

              }
        );


      var conexion3 = mysql.createConnection(credencialesBD);
      conexion3.query("INSERT INTO tbl_titulos_x_empleado(CODIGO_TITULO, CODIGO_EMPLEADO, "+
                                    "CODIGO_UNIVERSIDAD, FECHA, NUMERO_REGISTRO) "+
                                    "VALUES (?,?,?,sysdate(),?)",
                                  [
                                    peticion.body.codigoTitulo,
                                    id,
                                    peticion.body.codigoUniversidad,
                                    peticion.body.fechaTitulo,
                                    peticion.body.numeroRegistro
                                  ],
              function(error, informacion, campos){
                if (error) throw error;

              }
        );


    }

  });


});
////////////-----------------------------------DOCENTE---------------------------------------//////////////////
app.get('/slcTitularidad',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("SELECT CODIGO_TITULARIDAD, NOMBRE_TITULARIDAD FROM tbl_titularidad",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.get('/slcEspecialidad',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("SELECT CODIGO_ESPECIALIZACION, DESCRIPCION FROM tbl_area_especializaciones ",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});


app.get('/cargarPersonal2',function(peticion, respuesta){

  var conexion1 = mysql.createConnection(credencialesBD);
    var conexion2 = mysql.createConnection(credencialesBD);
    var memes = [];
    var sql = "select a.CODIGO_MAESTRO, "+
        "b.NOMBRE, b.APELLIDO, c.NOMBRE_GENERO,l.NOMBRE_CARGO,p.SUELDO_BASE,d.NOMBRE_LUGAR as LUGAR_NACIMIENTO, "+
        "e.NOMBRE_LUGAR as LUGAR_RESIDENCIA, f.NOMBRE_LUGAR as MUNICIPIO_NACIMIENTO, "+
        "g.NOMBRE_LUGAR as MUNICIPIO_RESIDENCIA, h.NOMBRE_CAMPUS, i.TIPO_IDENTIFICACION, j.NOMBRE_ESTADO_CIVIL, "+
        "b.CORREO_ELECTRONICO, b.DIRECCION, b.FECHA_NACIMIENTO, b.IDENTIDAD, b.TELEFONO, k.NOMBRE_TIPO_EMPLEADO, q.DESCRIPCION, t2.NOMBRE_TITULARIDAD, "+
        "t2.SALARIO_MAXIMO, t2.SALARIO_MINIMO "+
        "from tbl_maestros a "+
        "inner join tbl_personas b on a.CODIGO_MAESTRO = b.CODIGO_PERSONA "+
        "inner join tbl_empleados p on a.CODIGO_MAESTRO = p.CODIGO_EMPLEADO "+
        "inner join  tbl_generos c on b.CODIGO_GENERO = c.CODIGO_GENERO "+
        "inner join tbl_lugares d on b.CODIGO_LUGAR_NACIMIENTO = d.CODIGO_LUGAR "+
        "inner join tbl_lugares e on b.CODIGO_LUGAR_RESIDENCIA = e.CODIGO_LUGAR "+
        "inner join tbl_lugares f on b.CODIGO_MUNICIPIO_NACIMIENTO = f.CODIGO_LUGAR "+
        "inner join tbl_lugares g on b.CODIGO_MUNICIPIO_RESIDENCIA = g.CODIGO_LUGAR "+
        "inner join tbl_campus h on b.CODIGO_CAMPUS = h.CODIGO_CAMPUS "+
        "inner join tbl_tipo_identificacion i on b.CODIGO_TIPO_IDENTIFICACION = i.CODIGO_TIPO_IDENTIFICACION "+
        "inner join tbl_estados_civiles j on b.CODIGO_ESTADO_CIVIL = j.CODIGO_ESTADO_CIVIL "+
        "inner join tbl_cargos l on p.CODIGO_CARGO = l.CODIGO_CARGO "+
        "inner join tbl_tipo_empleado k on p.CODIGO_TIPO_EMPLEADO = k.CODIGO_TIPO_EMPLEADO "+
        "inner join tbl_area_especializaciones q on a.CODIGO_ESPECIALIZACION = q.CODIGO_ESPECIALIZACION "+
        "inner join tbl_titularidad t2 on a.CODIGO_TITULARIDAD = t2.CODIGO_TITULARIDAD";
    conexion1.query(sql)
    .on('result', function(meme){
        meme.comentarios=[];
        memes.push(meme);
        conexion1.pause();
        conexion2.query("select b.NOMBRE_TITULO "+
                          "from tbl_titulos_x_empleado a "+
                          "inner join tbl_titulos b on a.CODIGO_TITULO = b.CODIGO_TITULO "+
                          "where CODIGO_EMPLEADO=?",
                          [meme.CODIGO_MAESTRO])
        .on('result', function(comentario){
            meme.comentarios.push(comentario);
        })
        .on('end',function(){
            conexion1.resume();
        });
    })
    .on('end',function(){
        conexion1.end();
        conexion2.end();
        respuesta.send(memes);
});


});


app.post('/guardarPersonal1',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query(
  "INSERT INTO tbl_personas(CODIGO_PERSONA, CODIGO_GENERO, CODIGO_LUGAR_NACIMIENTO,"+
                            "CODIGO_LUGAR_RESIDENCIA, CODIGO_MUNICIPIO_NACIMIENTO,"+
                            "CODIGO_MUNICIPIO_RESIDENCIA, CODIGO_CAMPUS, CODIGO_TIPO_IDENTIFICACION,"+
                            "CODIGO_ESTADO_CIVIL, NOMBRE, APELLIDO, FECHA_NACIMIENTO, IDENTIDAD,"+
                            "DIRECCION, TELEFONO, CORREO_ELECTRONICO) "+
                            " VALUES "+
                            "(NULL,?,?,?,?,?,?,?,?,?,?,SYSDATE(),?,?,?,?)",
  [
    peticion.body.codigoGenero,
    peticion.body.lugarNacimiento,
    peticion.body.lugarResidencia,
    peticion.body.municioNacimiento,
    peticion.body.municioResidencia,
    peticion.body.campus,
    peticion.body.identificacion,
    peticion.body.estadoCivil,
    peticion.body.nombre,
    peticion.body.apellido,
    peticion.body.identidad,
    peticion.body.direccion,
    peticion.body.telefono,
    peticion.body.email
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      id=resultado.insertId;

      respuesta.send('correcto');

      var conexion2 = mysql.createConnection(credencialesBD);
      conexion2.query("INSERT INTO tbl_empleados(CODIGO_EMPLEADO, NUMERO_EMPLEADO, "
                                    +"PASSWORD, SUELDO_BASE, CODIGO_TIPO_EMPLEADO, CODIGO_CARGO) "
                                    +"VALUES (?,?,SHA1(?),?,1,?)",
                                  [
                                    id,
                                    peticion.body.numeroEmpleado,
                                    peticion.body.contrasenia,
                                    peticion.body.sueldo,
                                    peticion.body.cargoPesonal
                                  ],
              function(error, informacion, campos){
                if (error) throw error;

                var conexion3 = mysql.createConnection(credencialesBD);
                conexion3.query("INSERT INTO tbl_titulos_x_empleado(CODIGO_TITULO, CODIGO_EMPLEADO, "+
                                              "CODIGO_UNIVERSIDAD, FECHA, NUMERO_REGISTRO) "+
                                              "VALUES (?,?,?,sysdate(),?)",
                                            [
                                              peticion.body.codigoTitulo,
                                              id,
                                              peticion.body.codigoUniversidad,
                                              peticion.body.fechaTitulo,
                                              peticion.body.numeroRegistro
                                            ],
                        function(error, informacion, campos){
                          if (error) throw error;

                          var conexion4 = mysql.createConnection(credencialesBD);
                          conexion4.query("INSERT INTO tbl_maestros (CODIGO_MAESTRO, CODIGO_ESPECIALIZACION, CODIGO_TITULARIDAD) VALUES (?,?,?)",
                                                      [
                                                        id,
                                                        peticion.body.codigoEspecialidad,
                                                        peticion.body.codigotitularidad
                                                      ],
                                  function(error, informacion, campos){
                                    if (error) throw error;

                                  }
                            );
                        }
                  );

              }
        );

    }

  });

});

////////////------------------------------PERIODOS-----------------------------//////////////////
app.get('/slcTipoPeriodo',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("SELECT CODIGO_TIPO_PERIODO, TIPO_PERIODO FROM tbl_tipos_periodos",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/guardarPeriodo',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("INSERT INTO tbl_periodos(CODIGO_PERIODO, NOMBRE_PERIODO, FECHA_INICIO, FECHA_FIN, CODIGO_TIPO_PERIODO) VALUES (NULL,?,?,?,?)",
  [
    peticion.body.nombrePeriodo,
    peticion.body.fechaInicio,
    peticion.body.fechaFin,
    peticion.body.tipoPeriodo
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      respuesta.send('correcto');
      conexion.end();
    }

  });


});


app.get('/cargarPeriodos',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select  a.CODIGO_PERIODO, a.NOMBRE_PERIODO, CAST(DATE_FORMAT(a.FECHA_INICIO, '%Y-%m-%d') AS char(10))  as FECHA_INICIO, CAST(DATE_FORMAT(a.FECHA_FIN, '%Y-%m-%d') AS char(10))  as FECHA_FIN, b.TIPO_PERIODO "+
                  "from tbl_periodos a "+
                  "inner join tbl_tipos_periodos b on a.CODIGO_TIPO_PERIODO = b.CODIGO_TIPO_PERIODO",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});


app.post('/actualizarPeriodo',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("UPDATE tbl_periodos SET NOMBRE_PERIODO=?,FECHA_INICIO=?,FECHA_FIN=?,CODIGO_TIPO_PERIODO=? WHERE CODIGO_PERIODO=?",
            [
              peticion.body.nombrePeriodo,
              peticion.body.fechaInicio,
              peticion.body.fechaFin,
              peticion.body.tipoPeriodo,
              peticion.body.codigoPeriodo
            ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send('correcto');
            conexion.end();

          }
    );

});

////////////-----------------------------------PRUEBA----------------------------------------//////////////////
app.get('/cargarPlanEstudio',function(peticion, respuesta){

  var conexion1 = mysql.createConnection(credencialesBD);
    var conexion2 = mysql.createConnection(credencialesBD);
    var memes = [];
    var sql = "select a.CODIGO_CARRERA,c.NOMBRE_CARRERA,b.CODIGO_ASIGNATURA,b.CODIGO_ALTERNO,b.NOMBRE_ASIGNATURA, b.CANTIDAD_UNIDADES_VALORATIVAS"
                +" from asignatura_x_carrera a"
                +" inner join tbl_asignaturas b on a.CODIGO_ASIGNATURA = b.CODIGO_ASIGNATURA"
                +" inner join tbl_carreras c on a.CODIGO_CARRERA = c.CODIGO_CARRERA";
    conexion1.query(sql)
    .on('result', function(meme){
        meme.clases=[];
        memes.push(meme);
        conexion1.pause();
        conexion2.query("select a3.NOMBRE_ASIGNATURA"
                        +"  from tbl_requisitos a"
                        +"  inner join tbl_asignaturas a2 on a.CODIGO_ASIGNATURA = a2.CODIGO_ASIGNATURA"
                        +"  inner join tbl_asignaturas a3 on a.CODIGO_ASIGNATURA_REQUISITO = a3.CODIGO_ASIGNATURA"
                        +"  where a.CODIGO_ASIGNATURA=? and a.CODIGO_CARRERA=?",
                          [meme.CODIGO_ASIGNATURA,meme.CODIGO_CARRERA])
        .on('result', function(clase){
            meme.clases.push(clase);
        })
        .on('end',function(){
            conexion1.resume();
        });
    })
    .on('end',function(){
        conexion1.end();
        conexion2.end();
        respuesta.send(memes);
});


});

////////////-----------------------------------ALUMNO---------------------------------------//////////////////
app.post("/loginEstudiante", function(peticion, respuesta){
    var conexion = mysql.createConnection(credencialesBD);
    conexion.query("SELECT a.CODIGO_ALUMNO,a.NUMERO_CUENTA,b.CODIGO_CARRERA "
                    +" from tbl_alumnos a "
                    +" inner join tbl_carreras_x_alumnos b on b.CODIGO_ALUMNO=a.CODIGO_ALUMNO "+
                    "where PASSWORD=sha1(?) and NUMERO_CUENTA=?",
        [peticion.body.password, peticion.body.nCuenta],
        function(err, data, fields){
                if (data.length>0){
                    peticion.session.usuario = data[0].CODIGO_ALUMNO;
                    peticion.session.ncuenta = data[0].NUMERO_CUENTA;
                    peticion.session.carrera = data[0].CODIGO_CARRERA;
                    console.log(peticion.session.carrera);
                    data[0].estatus = 0;
                    respuesta.send(data[0]);
                }else{
                    respuesta.send({estatus:1, mensaje: "Login fallido"});
                }

         }
    );
});


app.post("/verificarFechaMatricula", function(peticion, respuesta){
    var conexion = mysql.createConnection(credencialesBD);
    conexion.query("SELECT IF (? BETWEEN a.FECHA_INICIO AND a.FECHA_FINAL, 'sí','no') AS respuesta from acceso_matricula a where a.acceso_matricula=1",
        [peticion.body.fechaActual],
        function(err, data, fields){
                    respuesta.send(data[0]);
         }
    );
});

app.post("/verificarVerCalificaciones", function(peticion, respuesta){
    var conexion = mysql.createConnection(credencialesBD);
    conexion.query("SELECT IF (? BETWEEN a.FECHA_INICIO AND a.FECHA_FINAL, 'sí','no') AS respuesta from acceso_matricula a where a.acceso_matricula=4",
        [peticion.body.fechaActual],
        function(err, data, fields){
                    respuesta.send(data[0]);
         }
    );
});




app.get('/slcDepartamentos',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select a2.CODIGO_DEPARTAMENTO,c.NOMBRE_CARRERA "+
                "from asignatura_x_carrera a "+
                "inner join tbl_asignaturas a2 on a.CODIGO_ASIGNATURA = a2.CODIGO_ASIGNATURA "+
                "inner join tbl_carreras c on a2.CODIGO_DEPARTAMENTO = c.CODIGO_CARRERA "+
                "where a.CODIGO_CARRERA=? "+
                "group by a2.CODIGO_DEPARTAMENTO",
  [
    peticion.session.carrera
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});


app.post('/slcClases',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select a.CODIGO_ASIGNATURA , CONCAT(b.CODIGO_ALTERNO,  ' ', b.NOMBRE_ASIGNATURA) as  NOMBRE_CLASE, b.CANTIDAD_UNIDADES_VALORATIVAS, c.NOMBRE_CARRERA as Departamento"
                  +" from asignatura_x_carrera a"
                  +" inner join tbl_asignaturas b on a.CODIGO_ASIGNATURA = b.CODIGO_ASIGNATURA"
                  +" inner join tbl_carreras c on b.CODIGO_DEPARTAMENTO = c.CODIGO_CARRERA"
                  +" where c.CODIGO_CARRERA=? and a.CODIGO_CARRERA=?",
  [
    peticion.body.codigoDepartamento,
    peticion.session.carrera
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/slcSecciones',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select a.CODIGO_ASIGNATURA,a.CODIGO_SECCION ,a.CODIGO_ALTERNO, a.DIAS, a.CANTIDAD_CUPOS, concat(c.NOMBRE,' ',c.APELLIDO) as DOCENTE "+
                  " from tbl_seccion a "+
                  " inner join acceso_matricula b on a.CODIGO_PERIODO=b.CODIGO_PERIODO "+
                  " inner join tbl_personas c on a.CODIGO_MAESTRO=c.CODIGO_PERSONA "+
                  " where a.CODIGO_ASIGNATURA=? and b.ACCESO_MATRICULA=1",
  [
    peticion.body.codigoClase
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/verificarRequisitos',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select count(*) requisitos "+
                  "from tbl_requisitos "+
                  "where CODIGO_ASIGNATURA=?",
  [
    peticion.body.codigoAsignatura
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});


app.post('/guardarMatricula',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("INSERT INTO tbl_matricula(FECHA_MATRICULA, CODIGO_SECCION, CODIGO_ALUMNO, CODIGO_ESTADO_MATRICULA) VALUES (sysdate(),?,?,1)",
  [
    peticion.body.codSeccion,
    peticion.session.usuario
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      respuesta.send('correcto');
      conexion.end();
            var conexion2 = mysql.createConnection(credencialesBD);
            conexion2.query("UPDATE tbl_seccion SET CANTIDAD_CUPOS = CANTIDAD_CUPOS-1 where CODIGO_SECCION=?",
            [
              peticion.body.codSeccion
            ],
            function(error, resultado){
              if (resultado.affectedRows==1){
                conexion2.end();

              }

            });

    }

  });


});

app.post('/verificarExistencia',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select count(*) as existe"
                  +" from tbl_matricula a"
                  +" inner join tbl_seccion t on a.CODIGO_SECCION = t.CODIGO_SECCION"
                  +" where a.CODIGO_ALUMNO=? and t.CODIGO_ASIGNATURA=?",
  [
    peticion.session.usuario,
    peticion.body.codigoAsignatura
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();

          }
    );

});

app.post('/cargarMatricula',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select b.CODIGO_SECCION,c.CODIGO_ALTERNO,c.NOMBRE_ASIGNATURA,c.CANTIDAD_UNIDADES_VALORATIVAS,"
                    +"   b.CODIGO_ALTERNO as SECCION,b.HORA_INICIO,b.HORA_FIN,d.NUMERO_AULA,e.NOMBRE_EDIFICIO,"
                    +"   concat(f.NOMBRE,' ',f.APELLIDO) as Docente, a.NOTA_TEMPORAL, g.NOMBRE_TIPO_EVALUACION"
                    +" from tbl_matricula a"
                    +" inner join tbl_seccion b on a.CODIGO_SECCION = b.CODIGO_SECCION"
                    +" inner join tbl_asignaturas c on b.CODIGO_ASIGNATURA = c.CODIGO_ASIGNATURA"
                    +" inner join tbl_aulas d on b.CODIGO_AULA = d.CODIGO_AULA"
                    +" inner join tbl_edificios e on d.CODIGO_EDIFICIO = e.CODIGO_EDIFICIO"
                    +" inner join tbl_personas f on f.CODIGO_PERSONA=b.CODIGO_MAESTRO"
                    +" left join tbl_tipos_evaluaciones g on a.EVALUACION_TEMPORAL = g.CODIGO_TIPO_EVALUACION"
                    +" where a.CODIGO_ALUMNO=?",
  [
    peticion.session.usuario
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/cancelarMatricula',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("delete from tbl_matricula where CODIGO_ALUMNO=? and CODIGO_SECCION=?",
  [
    peticion.session.usuario,
    peticion.body.codigoSeccion
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send('correcto');
            conexion.end();

                  var conexion2 = mysql.createConnection(credencialesBD);
                  conexion2.query("UPDATE tbl_seccion SET CANTIDAD_CUPOS = CANTIDAD_CUPOS+1 where CODIGO_SECCION=?",
                  [
                    peticion.body.codigoSeccion
                  ],
                  function(error, resultado){
                    if (resultado.affectedRows==1){
                      conexion2.end();

                    }

                  });

          }
    );

});

app.post('/cargarHistorial',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select concat(b.NOMBRE,' ',b.APELLIDO) as ESTUDIANTE,c.NUMERO_CUENTA,d.CODIGO_ALTERNO as SECCION, e.CODIGO_ALTERNO,"
                         +" e.NOMBRE_ASIGNATURA,e.CANTIDAD_UNIDADES_VALORATIVAS,f.NOMBRE_PERIODO, a.PROMEDIO,g.NOMBRE_TIPO_EVALUACION"
                  +" from tbl_historial a"
                  +" inner join tbl_personas b on b.CODIGO_PERSONA=a.CODIGO_ALUMNO"
                  +" inner join tbl_alumnos c on a.CODIGO_ALUMNO = c.CODIGO_ALUMNO"
                  +" inner join tbl_seccion d on a.CODIGO_SECCION = d.CODIGO_SECCION"
                  +" inner join tbl_asignaturas e on d.CODIGO_ASIGNATURA = e.CODIGO_ASIGNATURA"
                  +" inner join tbl_periodos f on a.CODIGO_PERIODO = f.CODIGO_PERIODO"
                  +" inner join tbl_tipos_evaluaciones g on a.CODIGO_ESTADO_EVALUAR = g.CODIGO_TIPO_EVALUACION"
                  +" where c.CODIGO_ALUMNO=?",
  [
    peticion.session.usuario
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});

////////////-----------------------------------ASIGNATURAS----------------------------------------//////////////////
app.get('/slcTipoAsignatura',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("SELECT CODIGO_TIPO_ASIGNATURA, TIPO_ASIGNATURA FROM tbl_tipo_asignatura",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});


app.post('/guardarAsignatura',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("INSERT INTO tbl_asignaturas(CODIGO_ASIGNATURA, CODIGO_ALTERNO, NOMBRE_ASIGNATURA, "
                  +"CANTIDAD_UNIDADES_VALORATIVAS, DIAS, CODIGO_DEPARTAMENTO, CODIGO_TIPO_ASIGNATURA) "
                  +"VALUES (NULL,?,?,?,NULL,?,?)",
  [
    peticion.body.codigoAsignatura,
    peticion.body.nombreAsignatura,
    peticion.body.cantidadUvAsignatura,
    peticion.body.codigoCarrera,
    peticion.body.codigoTipo
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      respuesta.send('correcto');
      conexion.end();
    }

  });
});

app.get('/cargarAsignaturas',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("SELECT a.CODIGO_ASIGNATURA, a.CODIGO_ALTERNO, a.NOMBRE_ASIGNATURA, a.CANTIDAD_UNIDADES_VALORATIVAS as UV, "
                      +"  b.NOMBRE_CARRERA "
                  +"FROM tbl_asignaturas a "
                  +"inner join tbl_carreras b on a.CODIGO_DEPARTAMENTO = b.CODIGO_CARRERA",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});


app.post('/carreraAsignatura',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("INSERT INTO asignatura_x_carrera (CODIGO_ASIGNATURA, CODIGO_CARRERA) VALUES (?,?)",
  [
    peticion.body.codigoAsignatura,
    peticion.body.carrera
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      respuesta.send('correcto');
      conexion.end();
    }

  });
});

app.post('/requisitoAsignatura',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("INSERT INTO tbl_requisitos (CODIGO_ASIGNATURA, CODIGO_ASIGNATURA_REQUISITO, CODIGO_CARRERA) VALUES (?,?,?)",
  [
    peticion.body.codigoAsignatura,
    peticion.body.requisito,
    peticion.body.carrera
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      respuesta.send('correcto');
      conexion.end();
    }

  });
});

////////////-----------------------------------DOCENTE----------------------------------------//////////////////

app.post("/loginEmpleado", function(peticion, respuesta){
    var conexion = mysql.createConnection(credencialesBD);
    conexion.query("select a.CODIGO_EMPLEADO,a.NUMERO_EMPLEADO,a.CODIGO_TIPO_EMPLEADO, CODIGO_CARGO "
                  +" from tbl_empleados a "
                  +" where PASSWORD=sha1(?) and a.NUMERO_EMPLEADO=?",
        [peticion.body.password, peticion.body.nEmpleado],
        function(err, data, fields){
                if (data.length>0){
                    peticion.session.nEmpleado = data[0].NUMERO_EMPLEADO;
                    peticion.session.cEmpleado = data[0].CODIGO_EMPLEADO;
                    console.log(peticion.session.nEmpleado);
                    data[0].estatus = 0;
                    respuesta.send(data[0]);
                }else{
                    respuesta.send({estatus:1, mensaje: "Login fallido"});
                }

         }
    );
});

app.post('/cargarClasesActuales',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select a.CODIGO_SECCION,a.CODIGO_ALTERNO,d.NOMBRE_ASIGNATURA "
                  +" from tbl_seccion a "
                  +" inner join acceso_matricula b on a.CODIGO_PERIODO=b.CODIGO_PERIODO "
                  +" inner join tbl_personas c on a.CODIGO_MAESTRO=c.CODIGO_PERSONA "
                  +" inner join tbl_asignaturas d on a.CODIGO_ASIGNATURA = d.CODIGO_ASIGNATURA "
                  +" where CODIGO_MAESTRO=?  and b.acceso_matricula=1",
  [
    peticion.session.cEmpleado
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});

app.post('/cargarAlumnosSeccion',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select b.NUMERO_CUENTA, c.NOMBRE,c.APELLIDO,c.TELEFONO,c.CORREO_ELECTRONICO,a.NOTA_TEMPORAL,a.EVALUACION_TEMPORAL"
                +" from tbl_matricula a"
                +" inner join tbl_alumnos b on a.CODIGO_ALUMNO = b.CODIGO_ALUMNO"
                +" inner join tbl_personas c on b.CODIGO_ALUMNO = c.CODIGO_PERSONA"
                +" where a.CODIGO_SECCION=?",
  [
    peticion.body.codSeccion
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});

app.post("/verificarIngresoCalificaciones", function(peticion, respuesta){
    var conexion = mysql.createConnection(credencialesBD);
    conexion.query("SELECT IF (? BETWEEN a.FECHA_INICIO AND a.FECHA_FINAL, 'sí','no') AS respuesta from acceso_matricula a where a.acceso_matricula=3",
        [peticion.body.fechaActual],
        function(err, data, fields){
                    respuesta.send(data[0]);
         }
    );
});


app.get('/cargarObs',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("SELECT CODIGO_TIPO_EVALUACION, NOMBRE_TIPO_EVALUACION FROM tbl_tipos_evaluaciones",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/cargarAlumnosSeccion2',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select b.CODIGO_ALUMNO,a.CODIGO_SECCION ,d.CODIGO_PERIODO,b.NUMERO_CUENTA, c.NOMBRE,c.APELLIDO,c.TELEFONO,"
                  +" c.CORREO_ELECTRONICO,a.NOTA_TEMPORAL,a.EVALUACION_TEMPORAL"
                  +" from tbl_matricula a"
                  +" inner join tbl_alumnos b on a.CODIGO_ALUMNO = b.CODIGO_ALUMNO"
                  +" inner join tbl_personas c on b.CODIGO_ALUMNO = c.CODIGO_PERSONA"
                  +" inner join tbl_seccion d on a.CODIGO_SECCION = d.CODIGO_SECCION"
                  +" where a.CODIGO_SECCION=?",
  [
    peticion.body.codSeccion
  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});

app.post('/actualizarMatricula',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("UPDATE tbl_matricula SET NOTA_TEMPORAL=?, EVALUACION_TEMPORAL=? WHERE CODIGO_SECCION=? and CODIGO_ALUMNO=?",
  [
    peticion.body.nota,
    peticion.body.tipoEvaluacion,
    peticion.body.seccion,
    peticion.body.alumno
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      respuesta.send('correcto');
      conexion.end();

                var conexion2 = mysql.createConnection(credencialesBD);
                conexion2.query("INSERT INTO tbl_historial (CODIGO_HISTORIAL, CODIGO_ALUMNO, CODIGO_SECCION, CODIGO_PERIODO,"
                                +" PROMEDIO, CODIGO_ESTADO_EVALUAR) VALUES (NULL,?,?,?,?,?)",
                [
                  peticion.body.alumno,
                  peticion.body.seccion,
                  peticion.body.periodo,
                  peticion.body.nota,
                  peticion.body.tipoEvaluacion
                ],
                function(error, resultado){
                  if (resultado.affectedRows==1){
                    conexion2.end();

                  }

                });

    }

  });
});

////////////-----------------------------------SECCIOENS---------------------------------------//////////////////
app.get('/slcAulas2',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("SELECT CODIGO_AULA, NUMERO_AULA FROM tbl_aulas",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.get('/slcAsignatura',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("SELECT CODIGO_ASIGNATURA, NOMBRE_ASIGNATURA FROM tbl_asignaturas",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.get('/slcMaestro',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select a.CODIGO_MAESTRO, concat(b.NOMBRE,' ',b.APELLIDO) as MAESTRO"
                  +" from tbl_maestros a"
                  +" inner join tbl_personas b on a.CODIGO_MAESTRO=b.CODIGO_PERSONA",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.get('/slcPeriodo',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("SELECT CODIGO_PERIODO, NOMBRE_PERIODO FROM tbl_periodos",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});



app.post('/guardarSeccion',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("INSERT INTO tbl_seccion(CODIGO_SECCION, CODIGO_ALTERNO, HORA_INICIO,"
                  +" HORA_FIN, DIAS, CODIGO_AULA, CODIGO_ASIGNATURA, CODIGO_MAESTRO, CODIGO_PERIODO, CANTIDAD_CUPOS)"
                  +" VALUES (NULL,?,?,?,?,?,?,?,?,?)",
  [
    peticion.body.codigoAlterno,
    peticion.body.horaInicio,
    peticion.body.horaFinal,
    peticion.body.dias,
    peticion.body.aula,
    peticion.body.asignatura,
    peticion.body.maestro,
    peticion.body.periodo,
    peticion.body.cupos,
  ],
  function(error, resultado){
    if (resultado.affectedRows==1){
      respuesta.send('correcto');
      conexion.end();
    }

  });
});


app.get('/cargarSecciones',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select a.CODIGO_SECCION,concat(c.NOMBRE,' ',c.APELLIDO) as DOCENTE,e.NOMBRE_PERIODO,"
                      +"  d.CODIGO_ASIGNATURA,d.NOMBRE_ASIGNATURA, d.CANTIDAD_UNIDADES_VALORATIVAS,a.CANTIDAD_CUPOS"
                +" from tbl_seccion a"
                +" inner join acceso_matricula b on b.CODIGO_PERIODO=a.CODIGO_PERIODO"
                +" inner join tbl_personas c on c.CODIGO_PERSONA=a.CODIGO_MAESTRO"
                +" inner join tbl_asignaturas d on a.CODIGO_ASIGNATURA = d.CODIGO_ASIGNATURA"
                +" inner join tbl_periodos e on a.CODIGO_PERIODO = e.CODIGO_PERIODO"
                +" where b.ACCESO_MATRICULA=1",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});


app.post('/actualizarSeccion',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("UPDATE tbl_seccion SET CANTIDAD_CUPOS=? WHERE CODIGO_SECCION=?",[
            peticion.body.cupos,
            peticion.body.codigoSeccion
            ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});


////////////-----------------------------------administracion---------------------------------------//////////////////
app.get('/cargarFechas',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select a.ACCESO_MATRICULA, tp.NOMBRE_PERIODO,CAST(DATE_FORMAT(a.FECHA_INICIO, '%Y-%m-%d') AS char(10))  as FECHA_INICIO,CAST(DATE_FORMAT(a.FECHA_FINAL, '%Y-%m-%d') AS char(10))  as FECHA_FINAL"
                  +" from acceso_matricula a"
                  +" inner join tbl_periodos tp on a.CODIGO_PERIODO = tp.CODIGO_PERIODO",
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});

app.post('/actualizarAcceso',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("UPDATE acceso_matricula SET CODIGO_PERIODO=?,FECHA_INICIO=?,FECHA_FINAL=? WHERE ACCESO_MATRICULA=?",[
            peticion.body.periodo,
            peticion.body.fecha1,
            peticion.body.fecha2,
            peticion.body.codigoFecha
            ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
          }
    );

});

app.post('/obtenerDatos',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select concat(a.NOMBRE,' ',a.APELLIDO) as DATOS"
                  +" from tbl_personas a"
                  +" where CODIGO_PERSONA=?",
                  [
                    peticion.session.usuario
                  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});

app.post('/obtenerDatos2',function(peticion, respuesta){
  var conexion = mysql.createConnection(credencialesBD);
  conexion.query("select concat(a.NOMBRE,' ',a.APELLIDO) as DATOS"
                  +" from tbl_personas a"
                  +" where CODIGO_PERSONA=?",
                  [
                    peticion.session.cEmpleado
                  ],
          function(error, informacion, campos){
            if (error) throw error;
            respuesta.send(informacion);
            conexion.end();
            console.log(informacion);
          }
    );

});


app.get("/logout",function(peticion, respuesta){
	peticion.session.destroy();
	respuesta.send("Sesion eliminada");
});


////////////-----------------------------------FIN----------------------------------------//////////////////
app.listen(4444);
